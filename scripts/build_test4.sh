cd ../pycam

image_name="tcbot_ros:test4"
dockerfile_name="Dockerfile.tcbot-test4"

docker build --progress=plain -f $dockerfile_name -t $image_name .
