docker run -it --rm \
-v /data:/data \
-v /home/nvidia/tcbot_ws:/home/nvidia/tcbot_ws \
-v /home/nvidia/ros_ws:/home/nvidia/ros_ws \
-v /home/nvidia/catkin_ws:/home/nvidia/catkin_ws \
-v /home/nvidia/camera_ws/go:/home/nvidia/camera_ws/go \
-v /home/nvidia/camera_ws/appfile:/home/nvidia/camera_ws/appfile \
-v /home/nvidia/camera_ws/pycam:/home/nvidia/camera_ws/pycam \
--env="DISPLAY" \
--env="QT_X11_NO_MITSHM=1" \
--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
-v $HOME/.Xauthority:/root/.Xauthority \
--net=host \
--cap-add="NET_ADMIN" \
--user nvidia \
--name test3_hik \
tcbot_ros:test3_hik