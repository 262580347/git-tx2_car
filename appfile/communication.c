#include <termios.h>            /* tcgetattr, tcsetattr */
#include <stdio.h>              /* perror, printf, puts, fprintf, fputs */
#include <unistd.h>             /* read, write, close */
#include <fcntl.h>              /* open */
#include <sys/signal.h>
#include <termios.h>
#include <sys/types.h>
#include <string.h>             /* bzero, memcpy */
#include <limits.h>             /* CHAR_MAX */
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <arpa/inet.h>
#include <time.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include <string.h>

#include <errno.h>

#include "cjson.h"

#define     UART_BUF_MAX        1024
#define     FIFO_SEND_PATH      "/home/gys/share/fifo_send"
#define     FIFO_REC_PATH      "/home/gys/share/fifo_rec"

int UartRecLength = 0, CjsonCount = 0;
unsigned char UartRecBuf[UART_BUF_MAX];
unsigned char CjsonBuf[UART_BUF_MAX];
unsigned char ReadFifoBuf[UART_BUF_MAX];
char WlanIp[30], LanIP[30];
int fd_FifoSend = 0, fd_FifoRec = 0;

static void HandleSig(int signo)
{
    if (SIGINT == signo || SIGTERM == signo)
    {
        printf("\033[0;31mprogram exit abnormally!\033[0;39m\n");
    }
    exit(0);
}

int getlocalip(char* eth0ip, char* wlan0ip)
{
    int i=0;
    int sockfd;
    struct ifconf ifconf;
    char buf[512];
    struct ifreq *ifreq;
    // struct ifreq buf[MAXINTERFACES];    
    // struct ifconf ifc;
    char* ip;
    char Count = 0;

    //初始化ifconf
    ifconf.ifc_len = 512;
    ifconf.ifc_buf = buf;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
    {
        return -1;
    }
    ioctl(sockfd, SIOCGIFCONF, &ifconf);    //获取所有接口信息
    close(sockfd);
    //接下来一个一个的获取IP地址
    int if_len = ifconf.ifc_len / sizeof(struct ifreq); 
    // printf("接口数量:%d\n", if_len);
    ifreq = (struct ifreq*)buf;
    
    for(int i=0; i<if_len; i++)
    {
        ip = inet_ntoa(((struct sockaddr_in*)&(ifreq->ifr_addr))->sin_addr);
        // printf("%s:%s\n", ifreq->ifr_name, ip);
        if(memcmp(ifreq->ifr_name, "eth0", 4) == 0)
        {
             strcpy(eth0ip,ip);
             Count++;
        }
        if(memcmp(ifreq->ifr_name, "wlan0", 5) == 0)
        {
             strcpy(wlan0ip,ip);
             Count++;
        }
        ifreq++;

    }
   
    if(Count >= 1)
    {
        return 0;
    }
    else
    {
        return -1;        
    }

}

 //Hi3519向STM32发送控制指令  0x03
 void Hi3519toSTM32Control(void)  // 0x03
 {
  	static unsigned int Seq = 0;
	 char *cJsonBuf;
	 char strTime[30];
	 cJSON *Root, *cjson_sub;
	 unsigned short MsgLen = 0;

	 Root = cJSON_CreateObject();
    cjson_sub = cJSON_CreateObject();
	 cJSON_AddNumberToObject(Root,"CMD",		 1);
	 cJSON_AddNumberToObject(Root,"Dir",		 0);
	 cJSON_AddNumberToObject(Root,"Seq",		 Seq++);
     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

	 cJSON_AddStringToObject(cjson_sub,"wlan", 	 WlanIp);
    cJSON_AddStringToObject(cjson_sub,"eth0", 	 LanIP);
    cJSON_AddNumberToObject(cjson_sub,"TFT", 	 1);

	 cJsonBuf = cJSON_PrintUnformatted(Root);

	 MsgLen = strlen(cJsonBuf);

	 cJSON_Delete(Root);

	 strcat(cJsonBuf, "\r");

	write(fd_FifoSend, cJsonBuf, MsgLen);

	 printf("\nMastertoSTM32Control(Size:%d):%s\n", MsgLen, cJsonBuf);

	 free(cJsonBuf);
 }
 //解析CJSON协议包
 int DecodecJsonMsg(char *cJsonBuf)
{
	cJSON *Root, *Temp, *Child, *tNode;
	cJSON *cjson_sub = NULL;
	cJSON *item =NULL;
	cJSON * cjson_arr = NULL;
	cJSON * cjson_arr_item = NULL;
	int cjson_arr_item_num = 0;

	char *Output, Err;

	unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
	unsigned int Seq = 0;
	unsigned int i;

    if(strlen((const char *)cJsonBuf) >= UART_BUF_MAX)
    {
        printf(" Data Length Over\n");
        return -1;
    }
	Root = cJSON_Parse((const char *)cJsonBuf);

	printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
	if (Root == NULL )
	{
	 printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "CMD");
	if(Temp != NULL)
	{
	 Cmd = Temp->valueint;
	}
	else
	{
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Dir");
	if(Temp != NULL)
	{
	 Dir = Temp->valueint;
	 
	}
	else
	{
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Seq");
	if(Temp != NULL)
	{
	 Seq = Temp->valueint;

	}
	else
	{
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Err");
	if(Temp != NULL)
	{

	}
	else
	{

	}
	 
    Child = cJSON_GetObjectItem(Root, "Msg");
    if((Child != NULL) && (Child->type == cJSON_Object))
    {
        tNode = cJSON_GetObjectItem(Child, "Vol");
        if(tNode != NULL)
        {

        }

        tNode = cJSON_GetObjectItem(Child, "Lev");
        if(tNode != NULL)
        {

        }				

        tNode = cJSON_GetObjectItem(Child, "Latitude");
        if(tNode != NULL)
        {
        
        }

        tNode = cJSON_GetObjectItem(Child, "Longitude");
        if(tNode != NULL)
        {
        
        }

			
	 }

	 Output = cJSON_PrintUnformatted(Root);



	 cJSON_Delete(Root);

// 	 printf("Out:%s\n\n",Output);

	 free(Output);

 	return 0;
 }

void FifoInit(void)
{
    if(access(FIFO_SEND_PATH,F_OK))/*判断是否已经创建了有名管道，如果已经创建，则返回0否则返回非0的数*/                      
    {
        int ret = mkfifo(FIFO_SEND_PATH,0777); /*创建有名管道,成功返回0,失败返回-1*/
        if(ret == -1) /*创建有名管道失败*/
        {
            perror("mkfifo");
            exit(1);
        }
    }

    if(access(FIFO_REC_PATH,F_OK))/*判断是否已经创建了有名管道，如果已经创建，则返回0否则返回非0的数*/                      
    {
        int ret = mkfifo(FIFO_REC_PATH,0777); /*创建有名管道,成功返回0,失败返回-1*/
        if(ret == -1) /*创建有名管道失败*/
        {
            perror("mkfifo");
            exit(1);
        }
    }

    int dummyfd = open(FIFO_SEND_PATH, O_RDONLY | O_NONBLOCK, 0);
    fd_FifoSend = open(FIFO_SEND_PATH, O_WRONLY | O_NONBLOCK, 0);
    if(fd_FifoSend  < 0) /*打开失败*/
    {
        perror("open");
        exit(1);
    }
    fd_FifoRec  = open(FIFO_REC_PATH, O_RDONLY | O_NONBLOCK, 0);
    if(fd_FifoRec < 0 ) /*打开失败*/
    {
        perror("open");
        exit(1);
    }
}

void ReadFifo(void)
{
    int ret = read(fd_FifoRec, ReadFifoBuf, sizeof(ReadFifoBuf));

    if(ret > 0)
    {
        printf("Send Fifo:%s\n", ReadFifoBuf);
        DecodecJsonMsg(ReadFifoBuf);
        memset(ReadFifoBuf, 0, sizeof(ReadFifoBuf));
    }
}

int main(int argc, char *argv[])
{
    int Count = 0;
    signal(SIGINT, HandleSig);
    signal(SIGTERM, HandleSig);

    getlocalip(LanIP, WlanIp);

    printf("Send Info\n");

    printf("Fifo Init\n");
    FifoInit();
    
        Hi3519toSTM32Control();
    while(1)
    {     
        ReadFifo();
        
        usleep(1000);
    }
}