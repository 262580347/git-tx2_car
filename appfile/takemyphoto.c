#include <termios.h>            /* tcgetattr, tcsetattr */
#include <stdio.h>              /* perror, printf, puts, fprintf, fputs */
#include <unistd.h>             /* read, write, close */
#include <fcntl.h>              /* open */
#include <sys/signal.h>
#include <termios.h>
#include <sys/types.h>
#include <string.h>             /* bzero, memcpy */
#include <limits.h>             /* CHAR_MAX */
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <arpa/inet.h>
#include <time.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include<string.h>
#include<errno.h>
#include "cjson.h"
#include "color.h"
#define		CMD_TAKE_PHOTO_X_Y		1
#define		CMD_TAKE_PHOTO_NOW		2
#define		CMD_TAKE_GO_X_Y		3
#define		CMD_CAMERA_RETURN		6
#define		CMD_RUNNING			7
#define		CMD_SET_PICPARM		8
#define		CMD_GET_PICPARM		9
#define		CMD_REBOOT			99
#define		CMD_HALT				100
#define     FILE_PATH       "/data/website/tcrobot/photos/PhotoStatus"
#define     PICPARM_PATH       "/data/website/tcrobot/photos/PicParm"
///data/website/tcrobot/photos 
//mount -o nolock,wsize=1024,rsize=1024 172.16.202.88:/data/website/tcrobot/photos /mnt/net
int UartRecLength = 0, CjsonCount = 0;
static unsigned char s_ManualFlag = 0;
static unsigned int s_DRC_EN = 0, s_WhiteLevel = 800000, s_BrightPr = 200, s_Compensation = 70, s_AEMaxGainRange = 1024;
static unsigned int s_PositionPoint = 0;
static void HandleSig(int signo)
{
    if (SIGINT == signo || SIGTERM == signo)
    {
        printf("\033[0;31mprogram exit abnormally!\033[0;39m\n");
    }
    exit(0);
}
int SetPhotoStatus(char *cmd)
{
	int ret;
	FILE *fp;
	char szLine[1024];
	if((fp = fopen(FILE_PATH, "wr")) == NULL)
	{
		perror("fopen()");
		return -1;
	}
	fprintf(fp, "%s", cmd);
	fclose(fp);
	return 1;
}
int SetPicParm(char *cmd)
{
	int ret;
	FILE *fp;
	char szLine[1024];
	if((fp = fopen(PICPARM_PATH, "wr")) == NULL)
	{
		perror("fopen()");
		return -1;
	}
	ftruncate(fp,0);
	fprintf(fp, "%s", cmd);
	fclose(fp);
	return 1;
}
 //TX向 Hi发送控制指令
 void TX2toHisiSendCmd(unsigned int AngularX, unsigned int AngularY, unsigned char Cmd)  // 0x03
 {
	static unsigned int Seq = 0;
	char *cJsonBuf;
	char strTime[100];
	cJSON *Root, *cjson_sub;
	unsigned short MsgLen = 0;
	time_t timep;
	struct tm *p;
	Root = cJSON_CreateObject();
	cjson_sub = cJSON_CreateObject();
	cJSON_AddNumberToObject(Root,"CMD",		 Cmd);
	cJSON_AddNumberToObject(Root,"Dir",		 0);
	time(&timep);	//系统时间
	p = localtime(&timep);
	sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	cJSON_AddStringToObject(Root,"Time",	strTime);
	cJSON_AddItemToObject(Root, "Msg", cjson_sub);
	cJSON_AddNumberToObject(cjson_sub,"X", 	 AngularX);
	cJSON_AddNumberToObject(cjson_sub,"Y", 	 AngularY);
	cJSON_AddNumberToObject(cjson_sub,"Point", 	 s_PositionPoint);
	if(s_ManualFlag)
	{
		cJSON_AddNumberToObject(cjson_sub,"DRC_EN", s_DRC_EN);
		cJSON_AddNumberToObject(cjson_sub,"WhiteLevel",s_WhiteLevel);
		cJSON_AddNumberToObject(cjson_sub,"BrightPr", 	 s_BrightPr);
		cJSON_AddNumberToObject(cjson_sub,"Compensation", s_Compensation);
		cJSON_AddNumberToObject(cjson_sub,"AEMaxGainRange", s_AEMaxGainRange);
	}
	cJsonBuf = cJSON_PrintUnformatted(Root);
	MsgLen = strlen(cJsonBuf);
	cJSON_Delete(Root);
	strcat(cJsonBuf, "\n");
	printf("\nMastertoHisiControl(Size:%d):%s\n", MsgLen, cJsonBuf);
	SetPhotoStatus(cJsonBuf);
	free(cJsonBuf);
 }
 //解析CJSON协议包
 int DecodecJsonMsg(char *cJsonBuf)
{
	cJSON *Root, *Temp, *Child, *tNode;
	cJSON *cjson_sub = NULL;
	cJSON *item =NULL;
	int ret = 0;
	char *Output, Err;
	unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
	unsigned int Seq = 0;
	unsigned int i;
    if(strlen((const char *)cJsonBuf) >= 1024)
    {
        printf(" Data Length Over\n");
        return -1;
    }
	Root = cJSON_Parse((const char *)cJsonBuf);
	// printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
	if (Root == NULL )
	{
	//  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
	 return -1;
	}
	Temp = cJSON_GetObjectItem(Root, "CMD");
	if(Temp != NULL)
	{
	    Cmd = Temp->valueint;
	}
	else
	{
	 return -1;
	}
	Temp = cJSON_GetObjectItem(Root, "Dir");
	if(Temp != NULL)
	{
	 Dir = Temp->valueint;
	}
	else
	{
	 return -1;
	}
    Child = cJSON_GetObjectItem(Root, "Msg");
    if((Child != NULL) && (Child->type == cJSON_Object))
    {
		tNode = cJSON_GetObjectItem(Child, "Status");
		if(tNode != NULL)
		{
			if(strncmp((char *)tNode->valuestring, "OK", 2) == 0)	
			{
				ret = CMD_TAKE_PHOTO_NOW;
			}
			else if(strncmp((char *)tNode->valuestring, "Ang", 3) == 0)	
			{
				ret = CMD_TAKE_GO_X_Y;
			}
			else if(strncmp((char *)tNode->valuestring, "Return", 6) == 0)	
			{
				ret = CMD_CAMERA_RETURN;
			}
			else if(strncmp((char *)tNode->valuestring, "Running", 7) == 0)	
			{
				ret = CMD_RUNNING;
			}
			else if(strncmp((char *)tNode->valuestring, "PicParm", 7) == 0)	
			{
				ret = CMD_SET_PICPARM;
			}
			else if(strncmp((char *)tNode->valuestring, "GetParm", 7) == 0)	
			{
				ret = CMD_GET_PICPARM;
			}
			else if(strncmp((char *)tNode->valuestring, "Reboot", 6) == 0)	
			{
				ret = CMD_REBOOT;
			}
			else if(strncmp((char *)tNode->valuestring, "Halt", 4) == 0)	
			{
				ret = CMD_HALT;
			}
     	}
		if(ret == CMD_GET_PICPARM)
		{
			SetPicParm(cJsonBuf);
			printf("%s\n", cJsonBuf);
		}
	}
	 Output = cJSON_PrintUnformatted(Root);
	 cJSON_Delete(Root);
	 free(Output);
 	return ret;
}
int GetPhotoStatus(void)
{
	int ret, i = 0;
	FILE *fp;
	char szLine[1024] = {0};
	int rtnval;
	if((fp = fopen(FILE_PATH, "r")) == NULL)
	{
		// perror("fopen()");
		return -1;
	}
	while(!feof(fp))
	{
		rtnval = fgetc(fp);
		if(rtnval == EOF)
		{
			break;
		}
		else
		{
			if(i > 1000)
			{
				break;
			}
			szLine[i++] = rtnval;				
		}
	}
    ret = DecodecJsonMsg(szLine);
	return ret;
}
int main(int argc, char *argv[])
{
    int X = 0, Y = 0;
	static unsigned status = 0;
	char strTime[100] = {0};
	time_t timep;
	struct tm *p;
	static unsigned int Count = 0;
    signal(SIGINT, HandleSig);
    signal(SIGTERM, HandleSig);
	time(&timep);	//系统时间
	p = localtime(&timep);
	sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	printf("%s ", strTime);
	if(argc == CMD_TAKE_PHOTO_X_Y)	//没有附加参数
	{
		printf(GREEN" 在当前点拍照\n"NONE);
		TX2toHisiSendCmd(0, 0, CMD_TAKE_PHOTO_NOW);
	}
	else if(argc == CMD_TAKE_PHOTO_NOW)	//一个附加参数
	{
		if(strncmp(argv[1], "Return", 6) == 0)	//CMD =6 返回原点指令
		{
			printf(GREEN" 相机返回原点\n"NONE);
			TX2toHisiSendCmd(0, 0, CMD_CAMERA_RETURN);
		}
		else if(strncmp(argv[1], "PicParm", 7) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 设置拍照参数\n");
			TX2toHisiSendCmd(0, 0, CMD_SET_PICPARM);
			s_ManualFlag = 0;
		}
		else if(strncmp(argv[1], "GetParm", 7) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 获取拍照参数\n");
			TX2toHisiSendCmd(0, 0, CMD_GET_PICPARM);
			s_ManualFlag = 0;
		}
		else if(strncmp(argv[1], "reboot", 6) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 重启设备\n");
			TX2toHisiSendCmd(0, 0, CMD_REBOOT);
		}
		else if(strncmp(argv[1], "halt", 4) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 关机\n");
			TX2toHisiSendCmd(0, 0, CMD_HALT);
			s_ManualFlag = 0;
		}
		else
		{
			sscanf(argv[1],"%d",&s_PositionPoint);
			if(s_PositionPoint > 0 && s_PositionPoint < 1000000)
			{
				printf("位置点:%d\n", s_PositionPoint);
				TX2toHisiSendCmd(0, 0, CMD_TAKE_PHOTO_NOW);
			}
			else
			{
				printf(" 错误的参数:%s\n", argv[1]);
				return -1;
			}
		}
	}
	else if(argc == 3)
	{
		sscanf(argv[1],"%d",&X);
		sscanf(argv[2],"%d",&Y);
		printf("Goto X:%d, Y:%d\n", X, Y);
    		TX2toHisiSendCmd(X, Y, CMD_TAKE_PHOTO_X_Y);
	}
	else if(argc == 4)
	{
		sscanf(argv[1],"%d",&X);
		sscanf(argv[2],"%d",&Y);
		if(strncmp((char *)argv[3], "go", 2) == 0)
		{
			printf("app转到指定角度 X:%d, Y:%d\n", X, Y);
    			TX2toHisiSendCmd(X, Y, CMD_TAKE_GO_X_Y);
		}
		else
		{
			printf("Goto X:%d, Y:%d\n", X, Y);
			sscanf(argv[3],"%d",&s_PositionPoint);
			if(s_PositionPoint > 0 && s_PositionPoint < 1000000)
			{
				printf("app位置点:%d\n", s_PositionPoint);
				TX2toHisiSendCmd(X, Y, CMD_TAKE_PHOTO_X_Y);
			}
			else
			{
				printf(" 错误的位置点参数:%s\n", argv[3]);
				return -1;
			}
		}
	}
	else if(argc == 7)
	{
		s_ManualFlag = 1;
		sscanf(argv[2],"%d",&s_DRC_EN);
		sscanf(argv[3],"%d",&s_WhiteLevel);
		sscanf(argv[4],"%d",&s_BrightPr);
		sscanf(argv[5],"%d",&s_Compensation);
		sscanf(argv[6],"%d",&s_AEMaxGainRange);
		printf("app设置拍照参数 DRCEN:%d, WhiteLevel:%d, BrightPr:%d, Compensation:%d, s_AEMaxGainRange:%d\n", s_DRC_EN, s_WhiteLevel, s_BrightPr, s_Compensation, s_AEMaxGainRange);
    		TX2toHisiSendCmd(0, 0, CMD_SET_PICPARM);
	}
	else if(argc == CMD_GET_PICPARM)
	{
    		TX2toHisiSendCmd(0, 0, CMD_GET_PICPARM);
	}
	else
	{
		printf("app错误的参数\n");
		return -1;
	}
	// X = strtoi(argv[1], 10);
	// Y = strtoi(argv[2], 10);
    while(1)
    {
		int ret = 0;
		ret = GetPhotoStatus();
		usleep(10000);
		Count++;
		if(Count >= 1000)
		{
			Count = 0;
			printf(RED" app指令执行超时，退出\n"NONE);
			return -1;
		}
        	if(ret == 7)
		{	
			if(status == 0)
				printf(YELLOW"正在拍照...\n"NONE);
			status = 1;
		}
		else if(ret == CMD_TAKE_PHOTO_NOW)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf(GREEN"拍照完成\n"NONE);
			return 0;
		}
		else if(ret == CMD_TAKE_GO_X_Y)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("转至指定角度指令已发送\n");
			return 0;
		}
		else if(ret == CMD_CAMERA_RETURN)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("返回原点指令已发送\n");
			return 0;
		}
		else if(ret == CMD_SET_PICPARM)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("修改拍照参数指令已发送\n");
			return 0;
		}
		else if(ret == CMD_GET_PICPARM)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("收到相机参数\n");
			return 0;
		}
		else if(ret == CMD_REBOOT)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("重启指令已发送\n");
			return 0;
		}
		else if(ret == CMD_HALT)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
			printf("%s ", strTime);
			printf("关机指令已发送\n");
			return 0;
		}
    }
}
