/*******************************************
    TX2 LED状态显示       
    作者：关宇晟  20200518
*******************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>   //define O_WRONLY and O_RDONLY
 
#define GPIO_HIGH       1
#define GPIO_LOW       0

void initGpio(int n)
{
    FILE * fp =fopen("/sys/class/gpio/export","w");
    if (fp == NULL)
        perror("export open filed");
    else
        fprintf(fp,"%d",n);
    fclose(fp);
}   //create gpio file
 
void setGpioDirection(int n,char *direction)
{
    char path[100] = {0};
    sprintf(path,"/sys/class/gpio/gpio%d/direction",n);
    FILE * fp =fopen(path,"w");
    if (fp == NULL)
        perror("direction open filed");
    else
        fprintf(fp,"%s",direction);
    fclose(fp);
}   //set gpio "in" or "out"
 
int getGpioValue(int n)
{
    char path[64];
    char value_str[3];
    int fd;
 
    snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/value", n);
    fd = open(path, O_RDONLY);
    if (fd < 0) {
        perror("Failed to open gpio value for reading!");
        return -1;
    }
 
    if (read(fd, value_str, 3) < 0) {
        perror("Failed to read value!");
        return -1;
    }
 
    close(fd);
    return (atoi(value_str));
}   //get gpio(n)'s value
 
void SetGpioOutValue(int n, char Val)
{
    char path[64];
    char value_str[3];
    int fd;
 
    snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/value", n);
    fd = open(path, O_RDWR);
    if (fd < 0) {
        perror("Failed to open gpio value for reading!");
        return ;
    }
    if(Val == GPIO_HIGH)
    {
        write(fd, "1", sizeof(Val));
    }
    else if(Val == GPIO_LOW)
    {
        write(fd, "0", sizeof(Val));
    }
    close(fd);
}

void SetGpioBlinkTime(int n)
{
    char path[64];
    char value_str[3];
    int fd, GpioVal = 0;
 
    snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/value", n);
    fd = open(path, O_RDWR);
    if (fd < 0) {
        perror("Failed to open gpio value for reading!");
        return ;
    }
 
    if (read(fd, value_str, 3) < 0) {
        perror("Failed to read value!");
        return ;
    }
    
    
    GpioVal = atoi(value_str);
    if(GpioVal == 0)
    {
        write(fd, "1", sizeof(char));
    }
    else if(GpioVal == 1)
    {
        write(fd, "0", sizeof(char));
    }
    close(fd);   
}

long CalInterval(long NewTime, long OldTime)
{
    if(NewTime > OldTime)
    {
        return (NewTime - OldTime);
    }
    else
    {
        return ( 1000000 + NewTime - OldTime);
    }
}
int main()
 
{
    static int lastretval = 0, nowval = 0;
    struct timeval tv;
    static long s_LastusecTime = 0, s_LastTime301 = 0, s_LastTime302 = 0;
    static int s_LED301DelayTime = 500000, s_LED302DelayTime = 100000;
    gettimeofday(&tv,NULL);
    s_LastusecTime = tv.tv_usec;
    printf("Now uSec:%ld \n",tv.tv_usec);
    initGpio(398);
    initGpio(300);

    setGpioDirection(398,"out");
    setGpioDirection(300,"out");
    printf(" Gpio Control\n");

    while(1)
    {
        gettimeofday(&tv,NULL);
        if(CalInterval(tv.tv_usec, s_LastTime301) > s_LED301DelayTime)
        {
            s_LastTime301 = tv.tv_usec;
            SetGpioBlinkTime(398);    
            SetGpioBlinkTime(300); 
        }


        usleep(10000);

    }
 
    return 0;
 
}
