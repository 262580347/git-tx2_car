/*******************************************
    巡园车车灯控制程序 
    作者：关宇晟  20200518
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
 
 #define		LED_STATUS_TYPE			9		//LED状态数据  颜色 状态 亮度
#define         TX2_ID                  0XB1
 //candump can0     接收测试
 int main(int argc, char *argv[])
{
    int s, nbytes;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;
    struct can_filter rfilter[1];

    unsigned char CheckSum = 0, CanBuf[8] = {0};
    int LEDColor = 0, LEDStatus = 0, LEDBright = 0;
    int Time = 1000;
    if(argc == 4)
 	{
 		sscanf(argv[1],"%d",&LEDColor);
        sscanf(argv[2],"%d",&LEDStatus);
         sscanf(argv[3],"%d",&LEDBright);
        if((LEDColor > 0 && LEDColor < 9) && (LEDStatus > 0 && LEDStatus < 8)  && (LEDBright > 0 && LEDBright < 8))
        {
            printf("车灯控制 颜色:%d | 状态:%d | 亮度:%d\n", LEDColor, LEDStatus, LEDBright);
        }
        else
        {
            printf("输入参数错误 ./carled 颜色(1-7) 状态(1-7) 亮度(1-7)\n");
            return -1;
        }
        
    }
    else
    {
        printf("输入参数错误 ./carled 颜色(1-7) 状态(1-7) 亮度(1-7)\n");
        return -1;
    }
    s = socket(PF_CAN, SOCK_RAW, CAN_RAW); //创建套接字
    strcpy(ifr.ifr_name, "can0" );

    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr)); //将套接字与 can0 绑定

    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

    frame.can_id = TX2_ID;
    frame.can_dlc = 8;
    frame.data[0] = LED_STATUS_TYPE;
    frame.data[1] = LEDColor;
    frame.data[2] = LEDStatus;
    frame.data[3] = LEDBright;
    frame.data[4] = 0;
    frame.data[5] = 0;
    frame.data[6] = 0;
    frame.data[7] = 0;
    CheckSum = 0;
    for(int i=0; i<7; i++)
    {
        CheckSum += frame.data[i];
    }
    frame.data[7] = CheckSum;
    frame.can_id = TX2_ID;
    nbytes = write(s, &frame, sizeof(frame)); //发送 frame[0]
    printf(" Can Send:");
    for(int i=0; i<8; i++)
    {
        printf(" 0x%02x", frame.data[i]);
        frame.data[i]++;
    }
    printf("\n");
    if(nbytes != sizeof(frame))
    {
        printf("Send Error frame\n!");
        close(s);
        return -1; //发送错误，退出
    }
  
        
    close(s);
    return 0;
}