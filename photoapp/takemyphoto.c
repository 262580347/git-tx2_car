#include <termios.h>            /* tcgetattr, tcsetattr */
#include <stdio.h>              /* perror, printf, puts, fprintf, fputs */
#include <unistd.h>             /* read, write, close */
#include <fcntl.h>              /* open */
#include <sys/signal.h>
#include <termios.h>
#include <sys/types.h>
#include <string.h>             /* bzero, memcpy */
#include <limits.h>             /* CHAR_MAX */
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <arpa/inet.h>
#include <time.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include<string.h>


#include<errno.h>

#include "cjson.h"


#define     FILE_PATH       "/home/gys/share/PhotoStatus"

int UartRecLength = 0, CjsonCount = 0;
 void TX2toHisiSendCmd(unsigned int AngularX, unsigned int AngularY, unsigned char Cmd);

static void HandleSig(int signo)
{
    if (SIGINT == signo || SIGTERM == signo)
    {
		// TX2toHisiSendCmd(0, 0, 3);
        printf("\033[0;31mprogram exit abnormally!\033[0;39m\n");
    }
    exit(0);
}


int SetPhotoStatus(char *cmd)
{
	int ret;
	FILE *fp;
	char szLine[1024];

	if((fp = fopen(FILE_PATH, "wr")) == NULL)
	{
		perror("fopen()");
		return -1;
	}
	
	fprintf(fp, "%s", cmd);
	fclose(fp);
	return 1;

}

 //TX向 Hi发送控制指令
 void TX2toHisiSendCmd(unsigned int AngularX, unsigned int AngularY, unsigned char Cmd)  // 0x03
 {
  	static unsigned int Seq = 0;
	 char *cJsonBuf;
	 char strTime[100];
	 cJSON *Root, *cjson_sub;
	 unsigned short MsgLen = 0;
	time_t timep;
	struct tm *p;

	 Root = cJSON_CreateObject();
    cjson_sub = cJSON_CreateObject();
	 cJSON_AddNumberToObject(Root,"CMD",		 Cmd);
	 cJSON_AddNumberToObject(Root,"Dir",		 0);

	 time(&timep);	//系统时间
	p = localtime(&timep);
	sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	 cJSON_AddStringToObject(Root,"Time",	strTime);

     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

    cJSON_AddNumberToObject(cjson_sub,"X", 	 AngularX);
    cJSON_AddNumberToObject(cjson_sub,"Y", 	 AngularY);

	 cJsonBuf = cJSON_PrintUnformatted(Root);

	 MsgLen = strlen(cJsonBuf);

	 cJSON_Delete(Root);

	strcat(cJsonBuf, "\n");
	 printf("\nMastertoHisiControl(Size:%d):%s\n", MsgLen, cJsonBuf);

     SetPhotoStatus(cJsonBuf);

	 free(cJsonBuf);
 }
 //解析CJSON协议包
 int DecodecJsonMsg(char *cJsonBuf)
{
	cJSON *Root, *Temp, *Child, *tNode;
	cJSON *cjson_sub = NULL;
	cJSON *item =NULL;
	int ret = 0;
	char *Output, Err;


	unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
	unsigned int Seq = 0;
	unsigned int i;

    if(strlen((const char *)cJsonBuf) >= 1024)
    {
        printf(" Data Length Over\n");
        return -1;
    }
	Root = cJSON_Parse((const char *)cJsonBuf);

	// printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
	if (Root == NULL )
	{
	//  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "CMD");
	if(Temp != NULL)
	{
	    Cmd = Temp->valueint;
	}
	else
	{
	 return -1;
	}

	Temp = cJSON_GetObjectItem(Root, "Dir");
	if(Temp != NULL)
	{
	 Dir = Temp->valueint;
	 
	}
	else
	{
	 return -1;
	}

	 
    Child = cJSON_GetObjectItem(Root, "Msg");
    if((Child != NULL) && (Child->type == cJSON_Object))
    {
        tNode = cJSON_GetObjectItem(Child, "Status");
        if(tNode != NULL)
        {
            if(strncmp((char *)tNode->valuestring, "Photo", 5) == 0)	
			{
				ret = 5;
			}
			else if(strncmp((char *)tNode->valuestring, "OK", 2) == 0)	
			{
				ret = 2;
			}
			else if(strncmp((char *)tNode->valuestring, "Running", 7) == 0)	
			{
				ret = 7;
			}
        }

	 }
	 Output = cJSON_PrintUnformatted(Root);
	 cJSON_Delete(Root);
	 free(Output);

 	return ret;
}

int GetPhotoStatus(void)
{
	int ret, i = 0;
	FILE *fp;
	char szLine[1024] = {0};
	int rtnval;

	if((fp = fopen(FILE_PATH, "r")) == NULL)
	{
		// perror("fopen()");
		return -1;
	}

	while(!feof(fp))
	{
		rtnval = fgetc(fp);
		if(rtnval == EOF)
		{
			break;
		}
		else
		{
			if(i > 1000)
			{
				break;
			}
			szLine[i++] = rtnval;				
		}
	}

    ret = DecodecJsonMsg(szLine);

	return ret;
}



int main(int argc, char *argv[])
{
    int Count = 0, X = 0, Y = 0;
	static unsigned status = 0;
	char strTime[100] = {0};
	time_t timep;
	struct tm *p;


    signal(SIGINT, HandleSig);
    signal(SIGTERM, HandleSig);


	time(&timep);	//系统时间
	p = localtime(&timep);
	sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);

	printf("%s ", strTime);
	if(argc == 1)
	{
		printf(" 在当前点拍照\n");

		TX2toHisiSendCmd(0, 0, 2);
	}
	else if(argc == 2)
	{
		printf(" 在当前点拍照\n");
		sscanf(argv[1],"%d",&X);
		Y = 0;

		printf("Goto X:%d, Y:%d\n", X, Y);
    	TX2toHisiSendCmd(X, Y, 1);
	}
	else if(argc == 3)
	{
		sscanf(argv[1],"%d",&X);

		sscanf(argv[2],"%d",&Y);

		printf("Goto X:%d, Y:%d\n", X, Y);
    	TX2toHisiSendCmd(X, Y, 1);
	}
	else
	{
		printf("错误的参数\n");
		return -1;
	}
	
	// X = strtoi(argv[1], 10);
	// Y = strtoi(argv[2], 10);

	static int s_Count = 0;
    while(1)
    {
		int ret = 0;
		ret = GetPhotoStatus();
		usleep(10000);
		s_Count++;
        if(ret == 7)
		{	
			if(status == 0)
				printf("正在拍照...\n");
			status = 1;
		}
		else if(ret == 2)
		{
			time(&timep);	//系统时间
			p = localtime(&timep);
			sprintf(strTime, "%02d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);

			printf("%s ", strTime);
			printf("拍照完成\n");
			return 0;
		}

		if(s_Count >= 1000)
		{
			s_Count = 0;
			printf("拍照等待超时,拍照失败\n");
			return 0;
		}
    }
}