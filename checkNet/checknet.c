#include <stdio.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int Geteth0Status(void)
{
	int i = 0;
	FILE *fp;
	char szLine[1024];
	int rtnval;

	if((fp = fopen("/sys/class/net/eth0/carrier", "r")) == NULL)
	{
		printf("open file fail\n");
		return -1;
	}

	while(!feof(fp))
	{
		rtnval = fgetc(fp);
		if(rtnval == EOF)
		{
			break;
		}
		else
		{
			if(i > 100)
			{
				break;
			}
			szLine[i++] = rtnval;				
		}
	}

	if(szLine[0] == 49)	//接入网线
	{
		fclose(fp);
		return 1;
	}
	else if(szLine[0] == 48)	//未接入网线
	{
		fclose(fp);
		return 0;
	}
	else	//未知状态
	{
		fclose(fp);
		return -2;
	}
}

int Getppp0Status(void)
{
	int i = 0;
	FILE *fp;
	char szLine[1024];
	int rtnval;

	if((fp = fopen("/sys/class/net/ppp0/carrier", "r")) == NULL)
	{
		printf("open file fail\n");
		return -1;
	}

	while(!feof(fp))
	{
		rtnval = fgetc(fp);
		if(rtnval == EOF)
		{
			break;
		}
		else
		{
			if(i > 100)
			{
				break;
			}
			szLine[i++] = rtnval;				
		}
	}

	if(szLine[0] == 49)	//接入网线
	{
		fclose(fp);
		return 1;
	}
	else if(szLine[0] == 48)	//未接入网线
	{
		fclose(fp);
		return 0;
	}
	else	//未知状态
	{
		fclose(fp);
		return -2;
	}
}

int main(void)
{
    int retval = 0;

    static char s_ppp0Status = 1, s_eth0Status = 1;
    int s, nbytes, i;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame[2] = {{0}};
    struct timeval tv;
    gettimeofday(&tv,NULL);
    // printf("当前系统秒数:%ld\n",tv.tv_sec);

    s = socket(PF_CAN, SOCK_RAW, CAN_RAW);//创建套接字
    strcpy(ifr.ifr_name, "can0" );
    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr));//将套接字与 can0 绑定
    //禁用过滤规则，本进程不接收报文，只负责发送
    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);
    //生成两个报文
    frame[0].can_id = 0x181;
    frame[0].can_dlc = 8;
    frame[0].data[0] = 0;
    frame[0].data[1] = 0;
    frame[0].data[2] = 0;
    frame[0].data[3] = 0;
    frame[0].data[4] = 0;
    frame[0].data[5] = 0;
    frame[0].data[6] = 0;
    frame[0].data[7] = 0;

    printf(" Can Send Test ...\r\n");
    //循环发送两个报文
    while(1)
    {
        retval = Geteth0Status();

        if(retval == -1)
        {
            printf("网口不存在\n");
            s_eth0Status = 2;
        }
        else  if(retval == 1)
        {
            printf("网线已接入\n");    
            s_eth0Status = 0;
        }
        else  if(retval == 0)
        {
            printf("网线未接入\n");   
            s_eth0Status = 1; 
        }
        else
        {
            printf("其他错误\n");
            s_eth0Status = 3;
        }
        
        retval = Getppp0Status();

        if(retval == -1)
        {
            printf("ppp0不存在\n");
            s_ppp0Status = 2;
        }
        else  if(retval == 1)
        {
            printf("ppp0已接入\n");   
            s_ppp0Status = 0; 
        }
        else  if(retval == 0)
        {
            printf("ppp0未接入\n");    
            s_ppp0Status = 1;
        }
        else
        {
            printf("ppp0其他错误\n");
            s_ppp0Status = 3;
        }

        frame[0].data[5] = s_eth0Status;
        frame[0].data[6] = s_ppp0Status;
        nbytes = write(s, &frame[0], sizeof(frame[0])); //发送 frame[0]
        printf(" Can Send:");
        for(i=0; i<8; i++)
        {
            printf(" 0x%02x", frame[0].data[i]);
        }
        printf("\n");
        if(nbytes != sizeof(frame[0]))
        {
            printf("Send Error frame[0]\n!");
            break; //发送错误，退出
        }
        sleep(1);
    }
    close(s);

    

}