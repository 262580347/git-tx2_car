#include <termios.h>            /* tcgetattr, tcsetattr */
 #include <stdio.h>              /* perror, printf, puts, fprintf, fputs */
 #include <unistd.h>             /* read, write, close */
 #include <fcntl.h>              /* open */
 #include <sys/signal.h>
 #include <termios.h>
 #include <sys/types.h>
 #include <string.h>             /* bzero, memcpy */
 #include <limits.h>             /* CHAR_MAX */
 #include <stdlib.h>
 #include <string.h>
 #include <ctype.h>
 #include <memory.h>
 #include <arpa/inet.h>
 #include <time.h>
 #include <netinet/in.h>
 #include <net/if.h>
 #include <sys/socket.h>
 #include <sys/ioctl.h>
 #include <sys/stat.h>
 #include <stdbool.h>
 #include <string.h>
 #include <dirent.h>
 #include <errno.h>
 #include <assert.h>
 #include "cjson.h"
//  #include "color.h"

//颜色定义
#define NONE                 "\e[0m"
#define BLACK                "\e[0;30m"
#define L_BLACK              "\e[1;30m"
#define RED                  "\e[0;31m"
#define L_RED                "\e[1;31m"
#define GREEN                "\e[0;32m"
#define L_GREEN              "\e[1;32m"
#define BROWN                "\e[0;33m"
#define YELLOW               "\e[1;33m"
#define BLUE                 "\e[0;34m"
#define L_BLUE               "\e[1;34m"
#define PURPLE               "\e[0;35m"     //紫色
#define L_PURPLE             "\e[1;35m"
#define CYAN                 "\e[0;36m"     //天蓝色
#define L_CYAN               "\e[1;36m"
#define GRAY                 "\e[0;37m"     //灰色
#define WHITE                "\e[1;37m"

#define 	RECOGNITION		0

 #define		CONFIGFILE_PATH	"/data/website/tcrobot/photos/cfg.ini"
 #define		YXB_CMD_PATH	"/home/nvidia/camera_ws/tcpServer/app "
 #define		YXB_RETURN_CMD		"/home/nvidia/camera_ws/tcpServer/app ret"
 #define     YXB_PATH        	"/data/website/tcrobot/photos/"
//  #define		HIK_CMD_PATH	"/home/nvidia/photoapp/camera_1200w.sh "
// #define		HIK_CMD_PATH	"/home/nvidia/photoapp/camera_1200w_redis.sh "
#define		HIK_ID  1
#define		HIK_CMD_PATH	"/home/nvidia/camera_ws/pycam/camera_B_1.sh "
 #define     HIK_PATH       	"/data/website/tcrobot/pic/"	
 #define     HIK_LIST_PATH   "/data/website/tcrobot/pic/"


 #define     PHOTONAME_FILE_PATH       "/data/website/tcrobot/photos/PhotoName"
 #define 	KEYVALLEN 		200
 #define 	SECTION_MAX_LEN 256
 #define 	STRVALUE_MAX_LEN 256
 #define 	LINE_CONTENT_MAX_LEN 256
 ///data/website/tcrobot/photos 
 //mount -o nolock,wsize=1024,rsize=1024 172.16.202.88:/data/website/tcrobot/photos /mnt/net
 static char sPathName[512] = HIK_PATH;
 static char sFileName[512] = "20201111_01";
 static char sYXBPathName[512] = YXB_PATH;
 static char sYXBFileName[512] = "20201111_01";
 static int s_Angle_Count = 0;
 static int s_Angle_X[10] = {0};
 static int s_Angle_Y[10] = {0};
 static int s_RunCount = 0;	//程序运行次数
 static char DeviceType = 0;//耘小宝设备类型
 static void HandleSig(int signo)
 {
     if (SIGINT == signo || SIGTERM == signo)
     {
         printf("\033[0;31mprogram exit abnormally!\033[0;39m\n");
     }
     exit(0);
 }
 char * l_trim(char * szOutput, const char *szInput)
 {
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	assert(szOutput != szInput);
 	for   (NULL; *szInput != '\0' && (isspace(*szInput)); ++szInput)
 	{
 		;
 	}
 	return strcpy(szOutput, szInput);
 }
 /*   删除右边的空格   */
 char *r_trim(char *szOutput, const char *szInput)
 {
 	char *p = NULL;
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	assert(szOutput != szInput);
 	strcpy(szOutput, szInput);
 	for(p = szOutput + strlen(szOutput) - 1; p >= szOutput && isspace(*p); --p)
 	{
 		;
 	}
 	*(++p) = '\0';
 	return szOutput;
 }
 /*   删除两边的空格   */
 char * a_trim(char * szOutput, const char * szInput)
 {
 	char *p = NULL;
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	l_trim(szOutput, szInput);
 	for   (p = szOutput + strlen(szOutput) - 1;p >= szOutput && isspace(*p); --p)
 	{
 		;
 	}
 	*(++p) = '\0';
 	return szOutput;
 }
 /*读取配置文件函数*/
 int GetProfileString(char *profile, char *AppName, char *KeyName, char *KeyVal )
 {
 	char appname[32],keyname[32];
 	char *buf,*c;
 	char buf_i[KEYVALLEN], buf_o[KEYVALLEN];
 	FILE *fp;
 	int found=0; /* 1 AppName 2 KeyName */
 	if( (fp=fopen( profile,"r" ))==NULL )
 	{
 		printf( "openfile [%s] error [%s]\n",profile,strerror(errno) );
 		return(-1);
 	}
 	fseek( fp, 0, SEEK_SET );
 	memset( appname, 0, sizeof(appname) );
 	sprintf( appname,"[%s]", AppName );
 	while( !feof(fp) && fgets( buf_i, KEYVALLEN, fp )!=NULL )
 	{
 		l_trim(buf_o, buf_i);
 		if( strlen(buf_o) <= 0 )
 		continue;
 		buf = NULL;
 		buf = buf_o;
 		if( found == 0 )
 		{
 			if( buf[0] != '[' ) 
 			{
 				continue;
 			} 
 			else if ( strncmp(buf,appname,strlen(appname))==0 )
 			{
 				found = 1;
 				continue;
 			}
 		} 
 		else if( found == 1 )
 		{
 			if( buf[0] == '#' )
 			{
 				continue;
 			} 
 			else if ( buf[0] == '[' ) 
 			{
 				break;
 			} 
 			else 
 			{
 				if( (c = (char*)strchr(buf, '=')) == NULL )
 				continue;
 				memset( keyname, 0, sizeof(keyname) );
 				sscanf( buf, "%[^=|^ |^\t]", keyname );
 				if( strcmp(keyname, KeyName) == 0 )
 				{
 					sscanf( ++c, "%[^\n]", KeyVal );
 					char *KeyVal_o = (char *)malloc(strlen(KeyVal) + 1);
 					if(KeyVal_o != NULL)
 					{
 						memset(KeyVal_o, 0, sizeof(KeyVal_o));
 						a_trim(KeyVal_o, KeyVal);
 						if(KeyVal_o && strlen(KeyVal_o) > 0)
 						{
 							strcpy(KeyVal, KeyVal_o);
 						}
 						free(KeyVal_o);
 						KeyVal_o = NULL;
 					}
 					found = 2;
 					break;
 				} 
 				else 
 				{
 					continue;
 				}
 			}
 		}
 	}
 	fclose( fp );
 	if( found == 2 )
 	return(0);
 	else
 	return(-1);
 }
 void IniReadValue(char* section, char* key, char* val, const char* file)
 {
     FILE* fp;
     int i = 0;
     int lineContentLen = 0;
     int position = 0;
     char lineContent[LINE_CONTENT_MAX_LEN] = {0};
     bool bFoundSection = false;
     bool bFoundKey = false;
     fp = fopen(file, "r");
     if(fp == NULL)
     {
         printf("%s: Opent file %s failed.\n", __FILE__, file);
         return;
     }
     while(feof(fp) == 0)
     {
         memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
         fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);
         if((lineContent[0] == ';') || (lineContent[0] == '\0') || (lineContent[0] == '\r') || (lineContent[0] == '\n'))
         {
             continue;
         }
         //check section
         if(strncmp(lineContent, section, strlen(section)) == 0)
         {
             bFoundSection = true;
             //printf("Found section = %s\n", lineContent);
             while(feof(fp) == 0)
             {
                 memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
                 fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);
                 //check key
                 if(strncmp(lineContent, key, strlen(key)) == 0)
                 {
                     bFoundKey = true;
                     lineContentLen = strlen(lineContent);
                     //find value
                     for(i = strlen(key); i < lineContentLen; i++)
                     {
                         if(lineContent[i] == '=')
                         {
                             position = i + 1;
                             break;
                         }
                     }
                     if(i >= lineContentLen) break;
                     strncpy(val, lineContent + position, strlen(lineContent + position));
                     lineContentLen = strlen(val);
                     for(i = 0; i < lineContentLen; i++)
                     {
                         if((lineContent[i] == '\0') || (lineContent[i] == '\r') || (lineContent[i] == '\n'))
                         {
                             val[i] = '\0';
                             break;
                         }
                     }  
                 }
                 else if(lineContent[0] == '[') 
                 {
                     break;
                 }
             }
             break;
         }
     }
     if(!bFoundSection){printf("No section = %s\n", section);}
     else if(!bFoundKey){printf("No key = %s\n", key);}
     fclose(fp);
 }
 int readStringValue(const char* section, char* key, char* val, const char* file)
 {
     char sect[SECTION_MAX_LEN] = {0};
     //printf("section = %s, key = %s, file = %s\n", section, key, file);
     if (section == NULL || key == NULL || val == NULL || file == NULL)
     {
         printf("%s: input parameter(s) is NULL!\n", __func__);
         return -1;
     }
     memset(sect, 0, sizeof(sect));
     sprintf(sect, "[%s]", section);
     //printf("reading value...\n");
     IniReadValue(sect, key, val, file);
     return 0;
 }
 int readIntValue(const char* section, char* key, const char* file)
 {
     char strValue[STRVALUE_MAX_LEN];
     memset(strValue, 0, sizeof(strValue));
     if(readStringValue(section, key, strValue, file) != 0)
     {
         printf("%s: error", __func__);
         return 0;
     }
     return(atoi(strValue));
 }
 void IniWriteValue(const char* section, char* key, char* val, const char* file)
 {
     FILE* fp;
     int i = 0, n = 0, err = 0;
     int lineContentLen = 0;
     int position = 0;
     char lineContent[LINE_CONTENT_MAX_LEN];
     char strWrite[LINE_CONTENT_MAX_LEN];
     bool bFoundSection = false;
     bool bFoundKey = false;
     memset(lineContent, '\0', LINE_CONTENT_MAX_LEN);
     memset(strWrite, '\0', LINE_CONTENT_MAX_LEN);
     n = sprintf(strWrite, "%s = %s\n", key, val);
     fp = fopen(file, "r+");
     if(fp == NULL)
     {
         printf("%s: Opent file %s failed.\n", __FILE__, file);
         return;
     }
     while(feof(fp) == 0)
     {
         memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
         fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);
         if((lineContent[0] == ';') || (lineContent[0] == '\0') || (lineContent[0] == '\r') || (lineContent[0] == '\n'))
         {
             continue;
         }
         //check section
         if(strncmp(lineContent, section, strlen(section)) == 0)
         {
             bFoundSection = true;
             while(feof(fp) == 0)
             {
                 memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
                 fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);
                 //check key
                 if(strncmp(lineContent, key, strlen(key)) == 0)
                 {
                     bFoundKey = true;
                     printf("%s: %s=%s\n", __func__, key, val);
                     fseek(fp, (0-strlen(lineContent)),SEEK_CUR);
                     err = fputs(strWrite, fp);
                     if(err < 0){printf("%s err.\n", __func__);}
                     break; 
                 }
                 else if(lineContent[0] == '[') 
                 {
                     break;
                 }
             }
             break;
         }
     }
     if(!bFoundSection){printf("No section = %s\n", section);}
     else if(!bFoundKey){printf("No key = %s\n", key);}
     fclose(fp);
 }
 int writeStringVlaue(const char* section, char* key, char* val, const char* file)
 {
     char sect[SECTION_MAX_LEN];
     //printf("section = %s, key = %s, file = %s\n", section, key, file);
     if (section == NULL || key == NULL || val == NULL || file == NULL)
     {
         printf("%s: input parameter(s) is NULL!\n", __func__);
         return -1;
     }
     memset(sect, '\0', SECTION_MAX_LEN);
     sprintf(sect, "[%s]", section);
     IniWriteValue(sect, key, val, file);
 }
 int writeIntValue(const char* section, char* key, int val, const char* file)
 {
     char strValue[STRVALUE_MAX_LEN];
     memset(strValue, '\0', STRVALUE_MAX_LEN);
     sprintf(strValue, "%d", val);
     writeStringVlaue(section, key, strValue, file);
 }
 int CheckConfigIni(void)
 {
 	char Value[32] = {0};
 	int i = 0, ret = 0;
 	char temp[10] = {0}, IP[30] = {0};

	// writeStringVlaue("Device_IP", "eth0ip", "123.456.798.666", CONFIGFILE_PATH);
 	// readStringValue("Device_IP", "eth0ip", IP, CONFIGFILE_PATH);
 	// printf("IP:%s \n", IP);
 	memset(Value, 0, sizeof(Value));
 	GetProfileString(CONFIGFILE_PATH, "PhotoCount", "Count", Value);
 	sscanf(Value,"%d",&s_Angle_Count);
 	if(s_Angle_Count >= 0 && s_Angle_Count <= 10)
 	{
 		printf("拍照点%d\n",s_Angle_Count);
 	}
 	else
 	{
 		printf("ini参数错误\n");
 		return -1;
 	}
 	if(s_Angle_Count >= 0 && s_Angle_Count <= 10)
 	{
 		for(i=0; i<s_Angle_Count; i++)
 		{
 			memset(Value, 0, sizeof(Value));
 			memset(temp, 0, sizeof(temp));
 			sprintf(temp, "X%d", i+1);
 			GetProfileString(CONFIGFILE_PATH, "Position", temp, Value);
 			sscanf(Value,"%d",&s_Angle_X[i]);
 			// s_Angle_X[i]= readIntValue( "Position", temp, CONFIGFILE_PATH);
 			memset(Value, 0, sizeof(Value));
 			sprintf(temp, "Y%d", i+1);
 			GetProfileString(CONFIGFILE_PATH, "Position", temp, Value);
 			sscanf(Value,"%d",&s_Angle_Y[i]);
 			// s_Angle_Y[i]= readIntValue( "Position", temp, CONFIGFILE_PATH);
 			printf("X%d:%d   		Y%d:%d\n", i+1, s_Angle_X[i], i+1, s_Angle_Y[i]);
 		}
 	}
 	else
 	{
 		printf("拍照点大于10\n");
 		return -1;
 	}
 	ret = GetProfileString(CONFIGFILE_PATH, "RunCount", "RunCnt", Value);
 	if(ret == -1)
 	{
 		printf("ini参数错误\n");
 		return -1;
 	}
 	else
 	{
 		sscanf(Value,"%d",&s_RunCount);
 		// printf("运行次数%d\n",s_RunCount);
 	}
 	s_RunCount++;
 	writeIntValue( "RunCount", "RunCnt", s_RunCount, CONFIGFILE_PATH);
 	return 0;
 }
 int ClearYXBPhotoList(char *cmd)
 {
 	int ret;
 	FILE *fp;
 	if((fp = fopen("/data/website/tcrobot/photos/photolist", "wr")) == NULL)
 	{
 		perror("fopen()");
 		return -1;
 	}
	 
 	ftruncate(fp, 0);
 	lseek(fp, 0, SEEK_SET);
 	// fprintf(fp, "%s", cmd);
 	fclose(fp);
 	return 1;
 	// system("echo "" > /data/website/tcrobot/photos/photolist");
 }

 int GetPhotoName(char *PhotoName)
 {
 	int ret, i = 0;
 	FILE *fp;
 	char szLine[1024] = {0};
 	int rtnval;
 	if((fp = fopen(PHOTONAME_FILE_PATH, "r")) == NULL)
 	{
 		// perror("fopen()");
 		return -1;
 	}
 	while(!feof(fp))
 	{
 		rtnval = fgetc(fp);
 		if(rtnval == EOF)
 		{
 			break;
 		}
 		else
 		{
 			if(i > 1000)
 			{
 				return -2;
 			}
 			PhotoName[i++] = rtnval;				
 		}
 	}
	return 0;
 }

 int WritePhotoList(char *path, char *cmd)	//图片名称追加操作
 {
 	FILE *fp;
 	if((fp = fopen(path, "a+")) == NULL)
 	{
 		perror("fopen() 不能操作挂载文件");
 		return -1;
 	}
 	fwrite(cmd, strlen(cmd), 1, fp);
 	char *next = "\n";
 	fwrite(next, strlen(next), 1, fp);
 	fclose(fp);
 	return 1;
 }

void WriteRedis(void)
{
	char AddCmd[200] = {0};
	char PhotoName[200] = {0};

	memset(AddCmd, 0, sizeof(AddCmd));
	memset(PhotoName, 0, sizeof(PhotoName));
	GetPhotoName(PhotoName);
	sprintf(AddCmd, "redis-cli -p 6380 -n 0 LPUSH %s '%d|%s%s/%s'", sFileName, DeviceType, YXB_PATH, sYXBFileName, PhotoName);
	system(AddCmd);
}
 int main(int argc, char *argv[])
 {
    DIR* dir = NULL;
	char Value[100] = {0};
	int Enable, WhiteLevel, BrightPr, ExpTimeRangeMin, AGainRange, Compensation;
 	int s_PhotoIndex = 0, s_PositionPoint = 0, i = 0, Temp = 0;//PhotoIndex巡园位置,s_PositionPoint当前点的拍照排序
 	char strTime[100] = {0};
 	char HikvisionFileName[100] = {0};
 	char HikvisionListName[100] = {0};
 	char HikvisionListPath[100] = {0};
 	char HikvisionCmd[100] = HIK_PATH;
 	char YXBListPath[100] = {0};
 	char YXBCmd[100] = YXB_CMD_PATH;
 	char sysCmd[200] = HIK_CMD_PATH;
 	char mvCmd[200] = YXB_PATH;
	char AddCmd[200] = {0};
	char PhotoName[200] = {0};
 	time_t timep;
 	struct tm *p;
 	static unsigned int Count = 0;
 	static bool b_EndFlag = false;
	static bool b_ManualPhoto = false, b_DirPhoto = false;
	int ret = 0;

	static int s_ManualHor = 0, s_ManualVer = 0;		//手动拍照的水平角和俯仰角
     signal(SIGINT, HandleSig);
     signal(SIGTERM, HandleSig);
 	time(&timep);	//系统时间
 	p = localtime(&timep);
 	printf(GREEN"拍照启动程序，版本V1.01 20210303 GYS\n"NONE);
 	sprintf(strTime, "%02d%02d%02d%02d%02d%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
 	printf(GREEN"当前系统时间%s\n"NONE, strTime);
 	CheckConfigIni();
 	if(argc == 3 || argc == 5)
 	{
 		// printf("Argc:%d %s %s\n", argc, argv[1], argv[2]);
 		if(strncmp(argv[2], "stop", 4) == 0)
 		{
 			printf("寻园结束命令!\n");
 			b_EndFlag = true;
 		}
		else if(strncmp(argv[2], "now", 3) == 0)
 		{
 			printf("当前角度手动拍照\n");
 			b_ManualPhoto = true;
			s_PhotoIndex = 0;
 		}
		else if(strncmp(argv[2], "dir", 3) == 0)
 		{
 			printf("指定角度手动拍照手动拍照,");
 			b_DirPhoto = true;
			s_PhotoIndex = 0;
			sscanf(argv[3],"%d",&s_ManualHor);
			sscanf(argv[4],"%d",&s_ManualVer);
			
 			if((s_ManualHor >= 0 && s_ManualHor <= 360) && (s_ManualVer >= 0 && s_ManualVer <= 110))
 			{
 				printf("Hor:%d, Ver:%d\n", s_ManualHor, s_ManualVer);
 			}
			else
			{
				printf("拍照角度设置错误！\n");
				return - 1;
			}		 
 		}
 		else
 		{
 			sscanf(argv[2],"%d",&s_PhotoIndex);
 			if(s_PhotoIndex > 999999 && s_PhotoIndex < 0)
 			{
 				printf("拍照点参数错误:%d\n", s_PhotoIndex);
 				return -1;
 			}
			 else
			 {
				 printf("巡园车位置:%d", s_PhotoIndex);
			 }
 		}
	
 		//海康目录
 		strcpy(sFileName, argv[1]);
 		strcat(sPathName,   sFileName);
 		// printf("Name:%s\n", sPathName);
 		dir = opendir(sPathName);
 		if((dir) == NULL) 
 		{ 
 			printf(RED"海康目标文件夹不存在，创建\n"NONE); 
 			 if(access(sPathName,   F_OK) != 0)  
 			{  
 				if(mkdir(sPathName,   0777) == -1)  
 				{   
 					perror("mkdir   error");   
 					return -1;   
 				}  
 				else
 				{
					chmod(sPathName, 0777);
 					printf("海康创建%s成功\n", sPathName);
 				}
 			}  
 		} 
 		else
 		{
 			// printf("海康目录已存在\n");
 		}
 		//耘小宝目录
 		strcpy(sYXBFileName, argv[1]);
 		strcat(sYXBPathName,   sYXBFileName);
 		// printf("YXBName:%s\n", sYXBPathName);
 		dir = opendir(sYXBPathName);
 		if((dir) == NULL) 
 		{ 
 			printf(RED"耘小宝目标文件夹不存在，创建\n"NONE); 
 			 if(access(sYXBPathName,   F_OK) != 0)  
 			{  
 				if(mkdir(sYXBPathName,   0777) == -1)  
 				{   
 					perror("mkdir   error");   
 					return   -1;   
 				}  
 				else
 				{
					chmod(sYXBPathName, 0777);
 					printf("耘小宝创建%s成功\n", sYXBPathName);
 					ClearYXBPhotoList(0);
 				}
 			}  
 		} 
 		else
 		{
 			// printf("耘小宝目录已存在\n");
 		}
		 #if (RECOGNITION == 1)
 		// 海康相机流程
 		if(b_EndFlag)
 		{
 			sprintf(HikvisionListPath, "%s%s/piclist", HIK_PATH, sFileName);	//列表路径
 			WritePhotoList(HikvisionListPath, "EOF");
			// memset(AddCmd, 0, sizeof(AddCmd));
			// sprintf(AddCmd, "redis-cli -n 0 LPUSH %s EOF", HikvisionListPath);
			// system(AddCmd);
 			// printf("列表路径:%s\n", HikvisionListPath);

 			sprintf(YXBListPath, "%s%s/photolist", YXB_PATH, sFileName);
 			WritePhotoList(YXBListPath, "EOF");
	
			memset(AddCmd, 0, sizeof(AddCmd));

			sprintf(AddCmd, "redis-cli -n 0 -p 6380 LPUSH %s EOF", sFileName);
			system(AddCmd);
			
 			printf("写入EOF\n");
			
 			return 0;
 		}
 		else
 		{
 			sleep(1);	//等待车稳定
 			strcat(HikvisionCmd,  sFileName);	//到达目录文件内
			 if(b_ManualPhoto == true || b_DirPhoto == true)
			 {
				 sprintf(HikvisionFileName, "/B_%d_%s_%d00.jpg %s 0&", s_PhotoIndex, strTime, HIK_ID, sFileName);
			 }
			 else
			 {
				 sprintf(HikvisionFileName, "/B_%d_%s_%d00.jpg %s 1&", s_PhotoIndex, strTime, HIK_ID, sFileName);
			 }
			 
 			
 			strcat(HikvisionCmd,  HikvisionFileName);		//目标文件	
 			
 			strcat(sysCmd,  HikvisionCmd);		//得到最终指令
			printf("HikvisionCmd:%s\n", sysCmd);
 			system(sysCmd);		//相机拍照指令
			
 			sprintf(HikvisionListName, "B_%d_%s.jpg", s_PhotoIndex, strTime);	//写入列表的名称
 			sprintf(HikvisionListPath, "%s%s/piclist", HIK_PATH, sFileName);	//列表路径
 			// printf("列表路径:%s\n", HikvisionListPath);
 			WritePhotoList(HikvisionListPath, HikvisionListName);
 		}
		 #endif
 		//耘小宝相机拍照；流程
 		if(s_Angle_Count == 1 || s_Angle_Count == 0 || b_ManualPhoto == true || b_DirPhoto == true)
 		{
			// printf("只拍一张\n");
			memset(YXBCmd, 0, sizeof(YXBCmd));
			if(b_DirPhoto == true)
			{
				sprintf(YXBCmd, "%s %d %d", YXB_CMD_PATH, s_ManualHor, s_ManualVer);	//执行拍照指令，调用app
			}
			else
			{
				sprintf(YXBCmd, "%s", YXB_CMD_PATH);	//执行拍照指令，调用app
			}
			printf("cmd:%s\n", YXBCmd);
 			ret = system(YXBCmd);
 			if(ret != 0)
			{
				printf(RED"go执行app超时，退出1\n"NONE);
				return -1;
			}
 			
 			sprintf(mvCmd, "mv %sA_* %s%s/", YXB_PATH, YXB_PATH, sYXBFileName);
 			// printf("YXB mv:%s\n", mvCmd);
 			system(mvCmd);
			

			//拍照完毕，写入识别列表
			if(b_ManualPhoto == false && b_DirPhoto == false)
			{
				#if (RECOGNITION == 1)
				WriteRedis();
				#endif
			}
 		}
 		else
 		{
 			if(s_RunCount >= 10)
 			{
 				system(YXB_RETURN_CMD);
 				printf("校准云台位置\n");
 				writeIntValue( "RunCount", "RunCnt", 0, CONFIGFILE_PATH);
 				sleep(6);
 				memset(YXBCmd, 0, sizeof(YXBCmd));
 				sprintf(YXBCmd, "%s%d %d %d", YXB_CMD_PATH, s_Angle_X[0], s_Angle_Y[0], s_PhotoIndex);
 				ret = system(YXBCmd);
				if(ret != 0)
				{
					printf(RED"go执行app超时，退出2\n"NONE);
					return -1;
				}

				
				//拍照完毕，写入识别列表
				#if (RECOGNITION == 1)
				WriteRedis();
				#endif
 			}
 			else
 			{
 				memset(YXBCmd, 0, sizeof(YXBCmd));
 				sprintf(YXBCmd, "%s%d", YXB_CMD_PATH, s_PhotoIndex);
 				ret = system(YXBCmd);
				printf("%s ret:%d\n", YXBCmd, ret);
				if(ret != 0)
				{
					printf(RED"go执行app超时，退出3\n"NONE);
					return -1;
				}

				//拍照完毕，写入识别列表
				#if (RECOGNITION == 1)
				WriteRedis();
				#endif
 			}
 			for(i=1; i<s_Angle_Count; i++)
 			{
 				memset(YXBCmd, 0, sizeof(YXBCmd));
				// s_PhotoIndex = i;	
 				sprintf(YXBCmd, "%s%d %d %d %d", YXB_CMD_PATH, s_Angle_X[i], s_Angle_Y[i], s_PhotoIndex, i);
 				printf("YXB CMD:%s\n", YXBCmd);
 				ret = system(YXBCmd);
				if(ret != 0)
				{
					printf(RED"go执行app超时，退出4\n"NONE);
					return -1;
				}
					
				//拍照完毕，写入识别列表
				#if (RECOGNITION == 1)
				WriteRedis();
				#endif
 			}
 			memset(YXBCmd, 0, sizeof(YXBCmd));
 			sprintf(YXBCmd, "%s%d %d go", YXB_CMD_PATH, s_Angle_X[0], s_Angle_Y[0]);
 			// printf("YXB CMD:%s\n", YXBCmd);
 			system(YXBCmd);
 			sprintf(mvCmd, "mv %sA_* %s%s/", YXB_PATH, YXB_PATH, sYXBFileName);
 			// printf("YXB mv:%s\n", mvCmd);
 			system(mvCmd);
 		}
 		char cpphotolistcmd[100] = {0};
 		sprintf(cpphotolistcmd, "cp %sphotolist %s%s/", YXB_PATH, YXB_PATH, sYXBFileName);
 		// printf("YXB cp:%s\n", cpphotolistcmd);
 		system(cpphotolistcmd);
 	}
 	else if((argc == 2) && (strncmp(argv[1], "parm", 4) == 0 || strncmp(argv[1], "Parm", 4) == 0))
 	{
 		memset(YXBCmd, 0, sizeof(YXBCmd));
		sprintf(YXBCmd, "%sset", YXB_CMD_PATH);
	//	printf("YXB CMD:%s\n", YXBCmd);
		system(YXBCmd);
		if(ret != 0)
		{
			printf(RED"go执行app修改参数超时，退出\n"NONE);
			return -1;
		}
 	}
 	else
 	{
 		printf("输入参数错误\n");
 		printf("./go +文件名 +位置点\n");
 		return -1;
 	}
 	return 0;
 }
 // point=$1
 // echo $point
 // path="/data/website/tcrobot/pic/Pic_"
 // time=$(date +%Y%m%d%H%M%S)
 // str1="${path}${point}_${time}.jpg"
 // echo $str1
 // sleep 1
 // /home/nvidia/photoapp/camera_1200w.sh $str1&
 // /home/nvidia/photoapp/app $point
 // /home/nvidia/photoapp/app 160 18 $point
 // /home/nvidia/photoapp/app 200 18 $point
 // /home/nvidia/photoapp/app 340 18 $point
 // /home/nvidia/photoapp/app 20 18 go
 