/*******************************************
    CAN 发送测试程序 
    作者：关宇晟  20200518
*******************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <sys/time.h>

int main()
{

    int s, nbytes, i;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame[2] = {{0}};
    struct timeval tv;
    gettimeofday(&tv,NULL);
    // printf("当前系统秒数:%ld\n",tv.tv_sec);

    s = socket(PF_CAN, SOCK_RAW, CAN_RAW);//创建套接字
    strcpy(ifr.ifr_name, "can0" );
    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr));//将套接字与 can0 绑定
    //禁用过滤规则，本进程不接收报文，只负责发送
    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);
    //生成两个报文
    frame[0].can_id = 0x11;
    frame[0].can_dlc = 8;
    frame[0].data[0] = 0;
    frame[0].data[1] = 0;
    frame[0].data[2] = 0;
    frame[0].data[3] = 0;
    frame[0].data[4] = 0;
    frame[0].data[5] = 0;
    frame[0].data[6] = 0;
    frame[0].data[7] = 0;

    printf(" Can Send Test ...\r\n");
    //循环发送两个报文
    while(1)
    {
        nbytes = write(s, &frame[0], sizeof(frame[0])); //发送 frame[0]
        printf(" Can Send:");
        for(i=0; i<8; i++)
        {
            printf(" 0x%02x", frame[0].data[i]);
            frame[0].data[i]++;
        }
        printf("\n");
        if(nbytes != sizeof(frame[0]))
        {
            printf("Send Error frame[0]\n!");
            break; //发送错误，退出
        }
        sleep(1);
    }
    close(s);
    return 0;
}
