/*******************************************
    CAN 接收测试程序 
    作者：关宇晟  20200518
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
 

#define		IMU_DATA_TYPE			1		//float
#define		RTK_DATA_TYPE			2		//float
#define		RADAR_DATA_TYPE			3		//int
#define		VOL_DATA_TYPE			4		//int
#define		LEVEL_STATUS_TYPE		5		//U8
#define		POWER_STATUS_TYPE		6		//U8
#define		TFT_STATUS_TYPE			7		//U8

#define		MPU_GYRO				10		//陀螺仪原始数据
#define		MPU_AAC					11		//重力传感器数据
#define		MPU_EULER				12		//欧拉角
#define		MPU_RESET				13		//重制MPU数据

#define		DATA_TYPE_FLOAT			1
#define		DATA_TYPE_INT			2
#define		DATA_TYPE_SHORT			3
#define		DATA_TYPE_U8			4

typedef struct
{
    int Range1;
	int Range2;
	int Range3;
	int Range4;
    float linear_acceleration_x, linear_acceleration_y, linear_acceleration_z; //加速度
	float angular_x, angular_y, angular_z;  //角度
	float angular_velocity_x, angular_velocity_y, angular_velocity_z;  //角速度
	float orientation_x, orientation_y, orientation_z, orientation_w;  //四元数据
	float MageneticRatio;  //磁场干扰百分比
    int Latitude;
	int Longitude;
	unsigned char RTKStatus;
    int DCPowerVol;
    unsigned char Level;
	unsigned char WLAN_IP[30];
	unsigned char LAN_IP[30];
}MasterDataType;

MasterDataType s_MasterDataType;

int main()
{
    int s, nbytes, ID=0, Count = 0, i;
    unsigned char RecBuf[8], CheckSum = 0, Index = 0, SensorID = 0;
    float f_data = 0;
    int val_int = 0;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;
    struct can_filter rfilter[1];

    short g_pitch, g_roll, g_yaw; 		//欧拉角
    short g_aacx, g_aacy, g_aacz;		//加速度传感器原始数据
    short g_gyrox, g_gyroy, g_gyroz;	//陀螺仪原始数据

    s = socket(PF_CAN, SOCK_RAW, CAN_RAW); //创建套接字
    strcpy(ifr.ifr_name, "can0" );

    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr)); //将套接字与 can0 绑定
    //定义接收规则，只接收表示符等于 0x11 的报文
    // rfilter[0].can_id = 0x11;
    // rfilter[0].can_mask = CAN_SFF_MASK;
    //设置过滤规则
    // setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
    printf(" Can Rec Test...\n");
    while(1)
    {
        nbytes = read(s, &frame, sizeof(frame)); //接收报文
        //显示报文
        if(nbytes > 0)
        {
            // printf("ID=0x%X DLC=%d :", frame.can_id, frame.can_dlc);
            // for(int i=0; i<frame.can_dlc; i++)
            // {
            //     printf(" 0x%02X", frame.data[i]);
            // }
            // printf("\n");
            Count ++;
            memcpy(RecBuf, &frame.data[0], 8);
            if(frame.can_id)
            {
                CheckSum = 0;
                for(i=0;i<7;i++)
                {
                    CheckSum += RecBuf[i];
                }
                
                if(CheckSum == RecBuf[7])
                {
                    SensorID  = RecBuf[0];
                    Index = RecBuf[1];
                    switch(SensorID)
                    {
                        case POWER_STATUS_TYPE:
                            
                        break;
                        
                        case LEVEL_STATUS_TYPE:                        
                            if(RecBuf[6] < 8)
                            {	
                                s_MasterDataType.Level = RecBuf[6];	
                            }
                            else
                            {
                                printf("Key Status Err\r\n");
                            }
                        break;
                        
                        case IMU_DATA_TYPE:

                            memcpy(&f_data, (void *)&RecBuf[3], sizeof(float));
                            switch(Index)
                            {
                                case 1:				
                                    s_MasterDataType.angular_x = f_data;
                                break;
                                
                                case 2:
                                    s_MasterDataType.angular_y = f_data;
                                break;
                                
                                case 3:
                                    s_MasterDataType.angular_z = f_data;
                                break;
                                
                                case 4:
                                    s_MasterDataType.linear_acceleration_x = f_data;
                                break;
                                
                                case 5:
                                    s_MasterDataType.linear_acceleration_y = f_data;
                                break;
                                
                                case 6:
                                    s_MasterDataType.linear_acceleration_z = f_data;
                                break;
                                
                                case 7:
                                    s_MasterDataType.angular_velocity_x = f_data;
                                break;
                                
                                case 8:
                                    s_MasterDataType.angular_velocity_y = f_data;
                                break;
                                
                                case 9:
                                    s_MasterDataType.angular_velocity_z = f_data;
                                break;
                                
                                case 10:
                                    s_MasterDataType.MageneticRatio = f_data;
                                break;
                                
                                case 11:
                                    s_MasterDataType.orientation_w = f_data;
                                break;
                                
                                case 12:
                                    s_MasterDataType.orientation_x = f_data;
                                break;
                                
                                case 13:
                                    s_MasterDataType.orientation_y = f_data;
                                break;
                                
                                case 14:
                                    s_MasterDataType.orientation_z = f_data;
                                break;
                            }						
                            
                        break;

                        case RADAR_DATA_TYPE:
                    
                            if(Index == 1)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Range1 = val_int;
                            }
                            else if(Index == 2)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Range2 = val_int;
                            }
                            else if(Index == 3)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Range3 = val_int;
                            }
                            else if(Index == 4)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Range4 = val_int;
                            }
                        break;

                        case VOL_DATA_TYPE:	//外部电压
                            memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                            s_MasterDataType.DCPowerVol = val_int;
                        break;
                        
                        case RTK_DATA_TYPE:	//RTK数据						
                            if(Index == 1)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Latitude = val_int;
                            }
                            else if(Index == 2)	
                            {
                                memcpy(&val_int, (void *)&RecBuf[3], sizeof(int));
                                s_MasterDataType.Longitude = val_int;
                            }
                            else if(Index == 3)	
                            {
                                s_MasterDataType.RTKStatus = RecBuf[6];
                            }
                            
                        break;
                            
                        case MPU_GYRO:
                            g_gyrox = (RecBuf[1] << 8) + RecBuf[2];
                            g_gyroy = (RecBuf[3] << 8) + RecBuf[4];
                            g_gyroy = (RecBuf[5] << 8) + RecBuf[6];
                            printf("gyro:%d %d %d\n", g_gyrox, g_gyroy, g_gyroz);
                        break;

                        case MPU_AAC:
                            g_aacx = (RecBuf[1] << 8) + RecBuf[2];
                            g_aacy = (RecBuf[3] << 8) + RecBuf[4];
                            g_aacz = (RecBuf[5] << 8) + RecBuf[6];
                            printf("aac:%d %d %d\n", g_aacx, g_aacy, g_aacz);
                        break;

                        case MPU_EULER:
                            g_pitch = ((RecBuf[1] << 8) + RecBuf[2]);
                            g_roll = (RecBuf[3] << 8) + RecBuf[4];
                            g_yaw = (RecBuf[5] << 8) + RecBuf[6];
                            printf("euler:%d %d %d\n", g_pitch, g_roll, g_yaw);
                        break;

                        default:
                            
                        break;
                    }
                }
                else
                {
                    printf("CheckSum Err,(ID 0x%04X) Rec:", ID);	
                    for(i=0;i<8;i++)
                    {
                        printf("%02X ", RecBuf[i]);
                    }	
                    printf("\n");
                }	
            }
        }

        if(Count >= 50)
        {
            Count = 0;
            
        }
    }
    close(s);
    return 0;
}