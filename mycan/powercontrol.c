/*******************************************
    CAN 接收测试程序 
    作者：关宇晟  20200518
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
 
 //candump can0     接收测试
 int main(int argc, char *argv[])
{
    int s, nbytes;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;
    struct can_filter rfilter[1];

    unsigned char CheckSum = 0, RecBuf[8] = {0};
    int PortNum = 0, PortStatus = 0;
    int Time = 1000;
    if(argc == 3)
 	{
 		sscanf(argv[1],"%d",&PortNum);
        sscanf(argv[2],"%d",&PortStatus);
        if(PortNum > 0 && PortNum < 7 && (PortStatus == 0 || PortStatus == 1))
        {
            printf("端口%d -> %d\n", PortNum, PortStatus);
        }
        else
        {
            printf("输入参数错误 端口号 0/1\n");
            return -1;
        }
        
    }
    else
    {
        printf("输入参数错误 端口号 0/1\n");
        return -1;
    }
    s = socket(PF_CAN, SOCK_RAW, CAN_RAW); //创建套接字
    strcpy(ifr.ifr_name, "can0" );

    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr)); //将套接字与 can0 绑定

    // printf(" Can Rec Test...\n");
    while(Time>0)
    {
        Time--;
        nbytes = read(s, &frame, sizeof(frame)); //接收报文
        //显示报文
        if(nbytes > 0)
        {
            // printf("ID=0x%X DLC=%d :", frame.can_id, frame.can_dlc);
            // for(int i=0; i<frame.can_dlc; i++)
            // {
            //     printf(" 0x%02X", frame.data[i]);
            // }
            // printf("\n");

            if(frame.can_id == 0xA1 && frame.can_dlc == 8 && frame.data[0] == 0x08)
            {
                memcpy(RecBuf, frame.data, sizeof(RecBuf));
                CheckSum = 0;
                for(int i=0; i<7; i++)
                {
                    CheckSum += RecBuf[i];
                }
                if(CheckSum == RecBuf[7])
                {
                    
                    if(RecBuf[PortNum] == PortStatus)
                    {
                        printf("状态不需要改变\n");
                    }
                    else
                    {
                        RecBuf[PortNum] = PortStatus;
                        CheckSum = 0;
                        for(int i=0; i<7; i++)
                        {
                            CheckSum += RecBuf[i];
                        }
                        RecBuf[7] = CheckSum;
                        memcpy(frame.data, RecBuf, sizeof(RecBuf));
                        frame.can_id = 0xB1;
                        nbytes = write(s, &frame, sizeof(frame)); //发送 frame[0]
                        printf(" Can Send:");
                        for(int i=0; i<8; i++)
                        {
                            printf(" 0x%02x", frame.data[i]);
                            frame.data[i]++;
                        }
                        printf("\n");
                        if(nbytes != sizeof(frame))
                        {
                            printf("Send Error frame\n!");
                            break; //发送错误，退出
                        }
                        printf("传感器状态: %d %d %d %d %d %d\n", RecBuf[1], RecBuf[2], RecBuf[3], RecBuf[4], RecBuf[5], RecBuf[6]);
                        break;
                    }
                    printf("传感器状态: %d %d %d %d %d %d\n", RecBuf[1], RecBuf[2], RecBuf[3], RecBuf[4], RecBuf[5], RecBuf[6]);
                    break;
                }
                else
                {
                    printf("Check Sum Err\n");
                    break;
                }
            }
        }
    }
    close(s);
    return 0;
}