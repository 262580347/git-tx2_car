/*******************************************
    CAN 接收测试程序 
    作者：关宇晟  20200518
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
 
 //candump can0     接收测试
int main()
{
    int s, nbytes;
    int val_int = 0;
    unsigned char CheckSum = 0;
    unsigned char CanBuf[8] = {0};
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;
    struct can_filter rfilter[1];
    s = socket(PF_CAN, SOCK_RAW, CAN_RAW); //创建套接字
    strcpy(ifr.ifr_name, "can0" );

    ioctl(s, SIOCGIFINDEX, &ifr); //指定 can0 设备
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    bind(s, (struct sockaddr *)&addr, sizeof(addr)); //将套接字与 can0 绑定
    //定义接收规则，只接收表示符等于 0x11 的报文
    rfilter[0].can_id = 0xA0;
    rfilter[0].can_mask = CAN_SFF_MASK;
    //设置过滤规则
    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
    printf(" 开启过滤规则，只接收0xA0数据...\n");
    while(1)
    {
        nbytes = read(s, &frame, sizeof(frame)); //接收报文
        //显示报文
        if(nbytes > 0)
        {
            // printf("ID=0x%X DLC=%d :", frame.can_id, frame.can_dlc);
            // for(int i=0; i<frame.can_dlc; i++)
            // {
            //     printf(" 0x%02X", frame.data[i]);
            // }
            // printf("\n");

            memcpy(CanBuf, frame.data, sizeof(CanBuf));

            CheckSum = 0;

            for(int i=0; i<7; i++)
            {
                CheckSum += CanBuf[i];
            }

            if(CheckSum == CanBuf[7])
            {
                switch(CanBuf[0])
                {
                    case 0x04:  //电压数据
                        val_int = 0;
                        memcpy(&val_int, (void *)&CanBuf[3], sizeof(int));
                        printf("电压:%dmV\n", val_int);
                    break;
                    
                    case 0x08:  //电压端口状态
                        printf("电压端口状态:%d,%d,%d,%d,%d,%d\n\n", CanBuf[1], CanBuf[2], CanBuf[3], CanBuf[4], CanBuf[5], CanBuf[6]);
                    break;

                    case 0x09:  //车灯状态和碰撞传感器状态
                        printf("车灯状态:%d,%d,%d,  碰撞传感器状态:%d,%d\n", CanBuf[1], CanBuf[2], CanBuf[3], CanBuf[4], CanBuf[5]);
                    break;

                    default:

                    break;
                }
            }
            else
            {
                printf("检验和错误\n");
            }
            
            
            
        }
    }
    close(s);
    return 0;
}