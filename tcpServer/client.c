#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "cjson.h"
#include <pthread.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>


#define SERVER_PORT 6699
#define   FILE_MAX  102400
#define   MAX_DATA  200

#define        SERVER_IP                "172.16.202.49"

#define        CMD_MASTER_SEND               0
#define        CMD_SLAVE_ACK                 1
#define        NOACK                         0
#define        RECACK                        1

#define        SEND_IDLE                     0
#define        SEND_BUSY                     1

#define		CMD_ALIVE				     0
#define		CMD_TAKEAPHOTO			     1
#define		CMD_GOTOTAKEAPHOTO		     2
#define		CMD_GOTOANGLE	               3
#define		CMD_RETURN		          4
#define		CMD_GETPARM			     5	
#define		CMD_SETPARM			     6	

typedef enum 
{
	ALIVE_NUM           = 0, 
	TAKEAPHOTO_NUM      = 1,
	GOTOTAKEAPHOTO_NUM  = 2,
	GOTOANGLE_NUM       = 3,
	RETURN_NUM		= 4,
     GETPARM_NUM         = 5,
     SETPARM_NUM         = 6,
     CJSON_NUM_MAX       
} 
CJSON_STRUCT;

typedef struct
{
	unsigned char 	CMD;
	unsigned char 	Dir;
	unsigned int 	Seq;
     unsigned char  Ack;
     unsigned char  isWorking;
}JSON_COMM_MSG; 

static JSON_COMM_MSG	cJsonMsgInfo[CJSON_NUM_MAX];		//JSON信息存储，用于重发、应答
static pthread_t s_RecvID, s_AliveID, s_TakeaPhotoID, s_GotoTakeaPhotoID, s_GotoAngleID, s_ReturnID, s_GetParmID, s_SetParmID;
int clientSocket;
int DecodecJsonMsg(char *cJsonBuf);
static unsigned char s_SocketStatus = 0, s_AliveAck = 0;
void ClientToServerAlive(unsigned int Seq);



void sig_handler(int signum)
{
     printf("client线程退出\n");
     pthread_cancel(s_RecvID);
     close(clientSocket);
     exit(1);
}

static void OnRxMatchcJsonMsg(JSON_COMM_MSG *cJsonMsg)
{
	if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_ALIVE].Seq) && cJsonMsg->CMD == CMD_ALIVE )	
	{
		printf(" 发送成功，Rec Ack, Kill the Alive\r\n");
		pthread_cancel(s_AliveID);
          cJsonMsgInfo[CMD_ALIVE].Ack = RECACK;
          cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_TAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_TAKEAPHOTO)	
	{
		printf(" Rec Ack, Kill the CMD_TAKEAPHOTO\r\n");
		// pthread_detach(s_TakeaPhotoID);
          cJsonMsgInfo[CMD_TAKEAPHOTO].Ack = RECACK;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_GOTOTAKEAPHOTO)	
	{
		printf(" Rec Ack, Kill the CMD_GOTOTAKEAPHOTO\r\n");
		// pthread_detach(s_GotoTakeaPhotoID);
           cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack = RECACK;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOANGLE].Seq) && cJsonMsg->CMD == CMD_GOTOANGLE)	
	{
		printf(" Rec Ack, Kill the CMD_GOTOANGLE\r\n");
		// pthread_detach(s_GotoAngleID);
           cJsonMsgInfo[CMD_GOTOANGLE].Ack = RECACK;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_RETURN].Seq) && cJsonMsg->CMD == CMD_RETURN)	
	{
		printf(" Rec Ack, Kill the CMD_RETURN\r\n");
		// pthread_detach(s_ReturnID);
           cJsonMsgInfo[CMD_RETURN].Ack = RECACK;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GETPARM].Seq) && cJsonMsg->CMD == CMD_GETPARM)	
	{
		printf(" Rec Ack, Kill the CMD_GETPARM\r\n");
		// pthread_detach(s_GetParmID);
           cJsonMsgInfo[CMD_GETPARM].Ack = RECACK;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_SETPARM].Seq) && cJsonMsg->CMD == CMD_SETPARM)	
	{
		printf(" Rec Ack, Kill the CMD_SETPARM\r\n");
		// pthread_detach(s_SetParmID);
           cJsonMsgInfo[CMD_SETPARM].Ack = RECACK;
	}
}

void *RecvPthread(void *arg)
{
     char recvbuf[MAX_DATA];
	printf("RecvPthread, Thread ID = %lu\n", s_RecvID);

     recvbuf[0] = '\0';
     int iDataNum;

     while(1)
     {
          iDataNum = recv(clientSocket, recvbuf, MAX_DATA, MSG_DONTWAIT); 
          if(iDataNum > 0)
          {
               recvbuf[iDataNum] = '\0';
               printf("Rec%s\n", recvbuf);
               DecodecJsonMsg(recvbuf);
               memset(recvbuf, 0, sizeof(recvbuf));
          }
          else if(iDataNum == 0)
          {
               // printf("Socket断开:%d\n", iDataNum);
               s_SocketStatus = 0;
          }
          usleep(10);
     }
     
	return NULL;
}

void *KeepAlivePthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("KeepAlivePthread, Thread ID = %lu\n", s_AliveID);
     cJsonMsgInfo[CMD_ALIVE].CMD = CMD_ALIVE;
	cJsonMsgInfo[CMD_ALIVE].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_ALIVE].Seq = s_Seq++;
     cJsonMsgInfo[CMD_ALIVE].Ack = NOACK;
     cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_ALIVE].Ack == NOACK)
               ClientToServerAlive(cJsonMsgInfo[CMD_ALIVE].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_ALIVE].Ack == RECACK)
          {
               printf("发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_IDLE;
          s_SocketStatus = 0;
          shutdown(clientSocket, SHUT_RDWR);
     }

     pthread_exit(NULL);
     
}

void TaskForKeepAlive(void)
{
     if(cJsonMsgInfo[CMD_ALIVE].isWorking == SEND_IDLE)
     {
          pthread_create(&s_AliveID, NULL, KeepAlivePthread, NULL);
          pthread_detach(s_AliveID);
     }
}

void ClientToServerAlive(unsigned int Seq)  // 客户端想服务的发送心跳包
{
     int ret = 0;
     char *cJsonBuf;
     char strTime[30];
     cJSON *Root, *cjson_sub;
     unsigned short MsgLen = 0;

     Root = cJSON_CreateObject();
     cjson_sub = cJSON_CreateObject();
     cJSON_AddNumberToObject(Root,"CMD",		 CMD_ALIVE);
     cJSON_AddNumberToObject(Root,"Dir",		 CMD_MASTER_SEND);
     cJSON_AddNumberToObject(Root,"Seq",		 Seq);
     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

     cJSON_AddStringToObject(cjson_sub,"Status", 	 "OK");

     cJsonBuf = cJSON_PrintUnformatted(Root);

     MsgLen = strlen(cJsonBuf);

     cJSON_Delete(Root);

     strcat(cJsonBuf, "\r");

     ret = send(clientSocket, cJsonBuf, MsgLen, MSG_NOSIGNAL);

     if(ret < 0)
     {
          printf("Socket 连接已经断开\n");
          shutdown(clientSocket, SHUT_RDWR);
          s_SocketStatus = 0;
     }

     printf("\nClientToServerAlive(%d)(Size:%d):%s\n", ret, MsgLen, cJsonBuf);

     free(cJsonBuf);
}
//发送指令应答包
void ClienttoServerAck(cJSON *Root)  
{
     char *cJsonBuf;
     cJSON *Temp, *cjson_sub;
     unsigned short MsgLen = 0;
     int Dir = 0;

     cJsonBuf = cJSON_PrintUnformatted(Root);

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == 0)
          {
               cJSON_ReplaceItemInObject(Root, "Dir", cJSON_CreateNumber(1));

               cJsonBuf = cJSON_PrintUnformatted(Root);

               MsgLen = strlen(cJsonBuf);

               cJSON_Delete(Root);

               strcat(cJsonBuf, "\r");

               send(clientSocket, cJsonBuf, MsgLen, MSG_NOSIGNAL);

               printf("Ack(Size:%d):%s\n", MsgLen, cJsonBuf);

               free(cJsonBuf);
          }
     }
     else
     {
          printf("没有Dir\n");
          return ;
     }
}
 //解析CJSON协议包
int DecodecJsonMsg(char *cJsonBuf)
{
     cJSON *Root, *cJsonAck, *Temp, *Child, *tNode;
     cJSON *cjson_sub = NULL;
     cJSON *item =NULL;
     int ret = 0;
     char *Output, Err;
     JSON_COMM_MSG cJsonMsg;

     unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
     unsigned int Seq = 0;
     unsigned int i;

     if(strlen((const char *)cJsonBuf) >= 1024)
     {
          printf(" Data Length Over\n");
          return -1;
     }
     Root = cJSON_Parse((const char *)cJsonBuf);
     cJsonAck = cJSON_Parse((const char *)cJsonBuf);
     // printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
     if (Root == NULL )
     {
     //  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "CMD");
     if(Temp != NULL)
     {
          Cmd = Temp->valueint;
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == CMD_MASTER_SEND)
          {
               ClienttoServerAck(cJsonAck);
          }
     }
     else
     {
          return -1;
     }
     Temp = cJSON_GetObjectItem(Root, "Seq");
     if(Temp != NULL)
     {
          Seq = Temp->valueint;
     }
     else
     {
          return -1;
     }

     if(Dir == CMD_MASTER_SEND)
     {

     } 
     else
     {
          cJsonMsg.CMD = Cmd;
          cJsonMsg.Dir = Dir;
          cJsonMsg.Seq = Seq;
          OnRxMatchcJsonMsg(&cJsonMsg);
     }
     

     Child = cJSON_GetObjectItem(Root, "Msg");
     if((Child != NULL) && (Child->type == cJSON_Object))
     {
          tNode = cJSON_GetObjectItem(Child, "Status");
          if(tNode != NULL)
          {
          
               if(strncmp((char *)tNode->valuestring, "OK", 2) == 0)	
               {

               }
               else if(strncmp((char *)tNode->valuestring, "Ang", 3) == 0)	
               {

               }
          }
               
     }
          Output = cJSON_PrintUnformatted(Root);
          cJSON_Delete(Root);
          free(Output);

     return ret;
}

int GetTCPStatus(void)
{
     struct tcp_info info;
     int len = sizeof(info);

     if(clientSocket>0)
     {
          getsockopt(clientSocket, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
          
          return info.tcpi_state;

     }
}

int main()
{
     //客户端只需要一个套接字文件描述符，用于和服务器通信
     int TcpStatus = 0;
     //描述服务器的socket
     struct sockaddr_in serverAddr;
     char sendbuf[MAX_DATA];
     static unsigned int Count = 1000;
     char FileBuf[FILE_MAX];
     int len = 0;
     FILE *fd;

     signal(SIGINT, sig_handler);
     signal(SIGTERM, sig_handler);

	

     if((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     {
        perror("socket");
       return 1;
     }
     serverAddr.sin_family = AF_INET;
     serverAddr.sin_port = htons(SERVER_PORT);
     //指定服务器端的ip，本地测试：127.0.0.1
     //inet_addr()函数，将点分十进制IP转换成网络字节序IP
     serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
     printf("服务器地址:%s 端口:%d\n", SERVER_IP, SERVER_PORT);
     while(connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
     {
          perror("connect");
          sleep(2);
     }

    
//      if( ( fd = fopen("/usr/app/tmpfs/snap_thm_1.jpg","rb") ) == NULL )
//      {
//         printf("File open.\n");
//         close(clientSocket);
//         exit(1);
//     }
//      printf("打开文件...\n");
//     bzero(FileBuf,sizeof(FileBuf));
//     while(!feof(fd))
//     {
//         len = fread(FileBuf, 1, sizeof(FileBuf), fd);
//         if(len != write(clientSocket, FileBuf, len))
//         {
//             printf("write Err.\n");
//             break;
//         }
//         else
//         {
             
//         }    
//     }
//     printf("传送文件完成\n");
//     close(clientSocket);
//     fclose(fd);

//     return 0;
     

     s_SocketStatus = 1;
     printf("连接到主机...\n");

     pthread_create(&s_RecvID, NULL, RecvPthread, NULL);
     pthread_detach(s_RecvID);
     while(1)
     {
          TcpStatus = GetTCPStatus();
          printf("Status:%d\n", TcpStatus);
          if(TcpStatus == TCP_ESTABLISHED || TcpStatus == TCP_LISTEN)
          {
               //Normal
          }
          else
          {
               s_SocketStatus = 0;
               shutdown(clientSocket, SHUT_RDWR);
          }

          if(s_SocketStatus == 1)
          {
               TaskForKeepAlive();
          }
          else
          {
               close(clientSocket);
               sleep(1);
               printf("尝试重新连接\n");
               if((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
               {
                    perror("socket");
                    return 1;
               }

               while(connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
               {
                    perror("connecting");
                    sleep(2);
               }
               printf("连接成功\n");
               s_SocketStatus = 1;
          }
          
          
         
          sleep(5);
          
     }
     close(clientSocket);
     return 0;
}
 