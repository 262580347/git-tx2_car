#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>
#include "cjson.h"
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#define   SERVER_TEXT_PORT 6699    //文本通信端口
#define   SERVER_FILE_PORT 6698    //文件收发端口
#define   SERVER_CMD_PORT 6697     //内部指令端口

 #define 	KEYVALLEN 		200
#define   MAX_DATA  1024
#define   FILE_MAX  102400
#define LINE_CONTENT_MAX_LEN 256

//颜色定义
#define NONE                 "\e[0m"
#define BLACK                "\e[0;30m"
#define L_BLACK              "\e[1;30m"
#define RED                  "\e[0;31m"
#define L_RED                "\e[1;31m"
#define GREEN                "\e[0;32m"
#define L_GREEN              "\e[1;32m"
#define BROWN                "\e[0;33m"
#define YELLOW               "\e[1;33m"
#define BLUE                 "\e[0;34m"
#define L_BLUE               "\e[1;34m"
#define PURPLE               "\e[0;35m"     //紫色
#define L_PURPLE             "\e[1;35m"
#define CYAN                 "\e[0;36m"     //天蓝色
#define L_CYAN               "\e[1;36m"
#define GRAY                 "\e[0;37m"     //灰色
#define WHITE                "\e[1;37m"

#define READ_CONFIG_ERR -1
#define READ_CONFIG_OK 	0

#define   CONFIG_FILE	"/data/website/tcrobot/photos/cfg.ini"
#define   PHOTOLISTPATH  "/data/website/tcrobot/photos/photolist"
 #define     PHOTONAME_FILE_PATH       "/data/website/tcrobot/photos/PhotoName"

#define        CMD_MASTER_SEND               0
#define        CMD_SLAVE_ACK                 1
#define        NOACK                         0
#define        RECACK                        1

#define        SEND_IDLE                     0
#define        SEND_BUSY                     1

#define		CMD_ALIVE				     0
#define		CMD_TAKEAPHOTO			     1
#define		CMD_GOTOTAKEAPHOTO		     2
#define		CMD_GOTOANGLE	               3
#define		CMD_RETURN		          4
#define		CMD_GETPARM			     5	
#define		CMD_SETPARM			     6	
#define		CMD_REBOOT			     7
#define		CMD_HALT				     8

typedef enum 
{
	ALIVE_NUM           = 0, 
	TAKEAPHOTO_NUM      = 1,
	GOTOTAKEAPHOTO_NUM  = 2,
	GOTOANGLE_NUM       = 3,
	RETURN_NUM		= 4,
     GETPARM_NUM         = 5,
     SETPARM_NUM         = 6,
     REBOOT_NUM          = 7,
     CJSON_NUM_MAX       
} 
CJSON_STRUCT;

typedef struct
{
	unsigned char 	CMD;
	unsigned char 	Dir;
	unsigned int 	Seq;
     unsigned char  Ack;
     unsigned char  isWorking;
}JSON_COMM_MSG; 

static JSON_COMM_MSG	cJsonMsgInfo[CJSON_NUM_MAX];		//JSON信息存储，用于重发、应答
static pthread_t s_RecvID, s_KeyboardID, s_TakeaPhotoID, s_GotoTakeaPhotoID, s_GotoAngleID, s_ReturnID, s_GetParmID, s_SetParmID, s_RebootID;
static pthread_t s_ServerFileID, s_ServerTextID, s_ServerCmdID, s_RecvCmdID;
static int s_Angle_X = 0, s_Angle_Y = 0, s_PhotoIndex = 0, s_PositionPoint = 0;
static unsigned int s_DRC_EN = 0, s_WhiteLevel = 800000, s_BrightPr = 200, s_ExpTimeRangeMin = 200, s_Compensation = 70, s_AEMaxGainRange = 1024, s_LumaVal = 100;
static char s_eth0IP[30] = {0};

int DecodecJsonMsg(char *cJsonBuf);
int DecodecJsonCmd(char *cJsonBuf);
void TaskForServerSendCmd(unsigned char Cmd);
void ServertoClientSendCMd(unsigned char Cmd, unsigned int Seq);
int GetTCPTextStatus(void);
int GetTCPFileStatus(void);
void *RecvTextPthread(void *arg);
void *RecvFilePthread(void *arg);
void *RecvCmdPthread(void *arg);
int ServerTextSocket, ServerFileSocket, ServerCmdSocket;
int TextClient, FileClient, CmdClient;
int s_SocketStatus = 0, s_CmdSocketStatus;

void GetSystemTimeStr( char * strTime )
{
	time_t timep;
	struct tm *p;
	
	time(&timep);
	p = localtime(&timep);
	sprintf(strTime, "%04d%02d%02d%02d%02d%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	return;
}

void GetSystemTimeStr2( char * strTime )
{
	time_t timep;
	struct tm *p;
	
	time(&timep);
	p = localtime(&timep);
	sprintf(strTime, "%04d-%02d-%02d %02d:%02d:%02d", (1900+p->tm_year),(1+p->tm_mon), p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	return;
}
void sig_handler(int signum)
{
     printf("server线程退出\n");
     pthread_cancel(s_RecvID);
     shutdown(TextClient, SHUT_RDWR);
     close(TextClient);
     close(ServerTextSocket);
     close(ServerFileSocket);
     exit(1);
}

 char * l_trim(char * szOutput, const char *szInput)
 {
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	assert(szOutput != szInput);
 	for   (NULL; *szInput != '\0' && (isspace(*szInput)); ++szInput)
 	{
 		;
 	}
 	return strcpy(szOutput, szInput);
 }
 /*   删除右边的空格   */
 char *r_trim(char *szOutput, const char *szInput)
 {
 	char *p = NULL;
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	assert(szOutput != szInput);
 	strcpy(szOutput, szInput);
 	for(p = szOutput + strlen(szOutput) - 1; p >= szOutput && isspace(*p); --p)
 	{
 		;
 	}
 	*(++p) = '\0';
 	return szOutput;
 }
 /*   删除两边的空格   */
 char * a_trim(char * szOutput, const char * szInput)
 {
 	char *p = NULL;
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	l_trim(szOutput, szInput);
 	for   (p = szOutput + strlen(szOutput) - 1;p >= szOutput && isspace(*p); --p)
 	{
 		;
 	}
 	*(++p) = '\0';
 	return szOutput;
 }
 /*读取配置文件函数*/
 int GetProfileString(char *profile, char *AppName, char *KeyName, char *KeyVal )
 {
 	char appname[32],keyname[32];
 	char *buf,*c;
 	char buf_i[KEYVALLEN], buf_o[KEYVALLEN];
 	FILE *fp;
 	int found=0; /* 1 AppName 2 KeyName */
 	if( (fp=fopen( profile,"r" ))==NULL )
 	{
 		printf( "openfile [%s] error [%s]\n",profile,strerror(errno) );
 		return(-1);
 	}
 	fseek( fp, 0, SEEK_SET );
 	memset( appname, 0, sizeof(appname) );
 	sprintf( appname,"[%s]", AppName );
 	while( !feof(fp) && fgets( buf_i, KEYVALLEN, fp )!=NULL )
 	{
 		l_trim(buf_o, buf_i);
 		if( strlen(buf_o) <= 0 )
 		continue;
 		buf = NULL;
 		buf = buf_o;
 		if( found == 0 )
 		{
 			if( buf[0] != '[' ) 
 			{
 				continue;
 			} 
 			else if ( strncmp(buf,appname,strlen(appname))==0 )
 			{
 				found = 1;
 				continue;
 			}
 		} 
 		else if( found == 1 )
 		{
 			if( buf[0] == '#' )
 			{
 				continue;
 			} 
 			else if ( buf[0] == '[' ) 
 			{
 				break;
 			} 
 			else 
 			{
 				if( (c = (char*)strchr(buf, '=')) == NULL )
 				continue;
 				memset( keyname, 0, sizeof(keyname) );
 				sscanf( buf, "%[^=|^ |^\t]", keyname );
 				if( strcmp(keyname, KeyName) == 0 )
 				{
 					sscanf( ++c, "%[^\n]", KeyVal );
 					char *KeyVal_o = (char *)malloc(strlen(KeyVal) + 1);
 					if(KeyVal_o != NULL)
 					{
 						memset(KeyVal_o, 0, sizeof(KeyVal_o));
 						a_trim(KeyVal_o, KeyVal);
 						if(KeyVal_o && strlen(KeyVal_o) > 0)
 						{
 							strcpy(KeyVal, KeyVal_o);
 						}
 						free(KeyVal_o);
 						KeyVal_o = NULL;
 					}
 					found = 2;
 					break;
 				} 
 				else 
 				{
 					continue;
 				}
 			}
 		}
 	}
 	fclose( fp );
 	if( found == 2 )
 	return(0);
 	else
 	return(-1);
 }
/**********************************************************************
*	读出指定的段名和项的值
**********************************************************************/
int ReadConfigIni(const char* psection, const char* pkey, int* pval)
{
    FILE* fp;
    int i = 0;
    int lineContentLen = 0;
    int position = 0;
    char lineContent[LINE_CONTENT_MAX_LEN];
    int bFoundSection = 0;
    int bFoundKey = 0;
	char key[100];
	char section[100];
	char str_val[100];
	
	if (psection == NULL || pkey == NULL || pval == NULL)
    {
        printf("%s: input parameter(s) is NULL!\n", __func__);
        return READ_CONFIG_ERR;
    }

	sprintf(section, "[%s]", psection);
	//strcpy(section,psection);
    strcpy(key, pkey);
	
    fp = fopen(CONFIG_FILE, "r");
    if(fp == NULL)
    {
        printf("%s: Opent file %s failed.\n", __FILE__, CONFIG_FILE);
        return READ_CONFIG_ERR;
    }
    while(feof(fp) == 0)	//判断是否到文件末尾
    {
        memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
        fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);	//读一行
        if(	(lineContent[0] == ';') || 
			(lineContent[0] == '\0') || 
			(lineContent[0] == '\r') || 
			(lineContent[0] == '\n'))
        {
            continue;
        }

        //check section
        if(strncmp(lineContent, section, strlen(section)) == 0)
        {
            bFoundSection = 1;
            //printf("Found section = %s\n", lineContent);
            while(feof(fp) == 0)	//判断是否到文件末尾
            {
                memset(lineContent, 0, LINE_CONTENT_MAX_LEN);
                fgets(lineContent, LINE_CONTENT_MAX_LEN, fp);	//读一行
				
				// 判断是否是注释行(以;开头的行就是注释行)
                if (lineContent[0] == ';')
                {
                    continue;
                }
                //check key
                if(strncmp(lineContent, key, strlen(key)) == 0)
                {
                    bFoundKey = 1;
                    lineContentLen = strlen(lineContent);
                    //find value
                    for(i = strlen(key); i < lineContentLen; i++)
                    {
                        if(lineContent[i] == '=')
                        {
                            position = i + 1;
                            break;
                        }
                    }
                    if(i >= lineContentLen) break;
					
					memset(str_val,'\0',sizeof(str_val));
                    strncpy(str_val, lineContent + position, strlen(lineContent + position));	//key的值
                    lineContentLen = strlen(str_val);
                    for(i = 0; i < lineContentLen; i++)
                    {
                        if((str_val[i] == '\0') || (str_val[i] == '\r') || (str_val[i] == '\n'))
                        {
                            str_val[i] = '\0';
                            break;
                        }
                    } 
					//strncpy(pval, str_val, strlen(val));	//输出key的值
					*pval = atoi(str_val);
                }
                else if(lineContent[0] == '[') 
                {
                    break;
                }
            }
            break;
        }
    }
	fclose(fp);
    if( (bFoundSection==1) && (bFoundKey==1))
	{
		//printf("No section = %s\n", section);
		return READ_CONFIG_OK; 
	}
    else
	{
		return READ_CONFIG_ERR;
	}
    	
}

void *ServerTextPthread(void* arg)
{
     int TcpStatus = 0;
     struct sockaddr_in server_addr;
     struct sockaddr_in clientAddr;
     int addr_len = sizeof(clientAddr);
     
     char buffer[MAX_DATA];
     int iDataNum;
     static unsigned int Count = 0;
     char  FIleBuf[FILE_MAX];
     FILE *fp;
     int  filelen;
     char strtime[30] = {0}, FileName[40] = {0};

     printf("ServerTextPthread, Thread ID = %lu\n", pthread_self());
     //socket函数，失败返回-1
     //int socket(int domain, int type, int protocol);
     //第一个参数表示使用的地址类型，一般都是ipv4，AF_INET
     //第二个参数表示套接字类型：tcp：面向连接的稳定数据传输SOCK_STREAM
     //第三个参数设置为0
     if((ServerTextSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     {
          perror("socket");
          return 0;
     }

     int opt = 1;
     setsockopt(ServerTextSocket,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof( opt ));
     //socket程序退出后端口依然被占用问题的解决

     bzero(&server_addr, sizeof(server_addr));
     //初始化服务器端的套接字，并用htons和htonl将端口和地址转成网络字节序
     server_addr.sin_family = AF_INET;
     server_addr.sin_port = htons(SERVER_TEXT_PORT);
     //ip可是是本服务器的ip，也可以用宏INADDR_ANY代替，代表0.0.0.0，表明所有地址
     server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
     //对于bind，accept之类的函数，里面套接字参数都是需要强制转换成(struct sockaddr *)
     //bind三个参数：服务器端的套接字的文件描述符，
     if(bind(ServerTextSocket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
     {
          perror("connect");
          close(ServerTextSocket);
          return 0;
     }
     //设置服务器上的socket为监听状态
     // printf("设置服务器上的socket为监听状态\n");
     if(listen(ServerTextSocket, 5) < 0)
     {
          perror("listen");
          return 0;
     }

     while(1)
     {
          printf("cjson指令监听端口: %d\n", SERVER_TEXT_PORT);
          //调用accept函数后，会进入阻塞状态
          //accept返回一个套接字的文件描述符，这样服务器端便有两个套接字的文件描述符，
          //serverSocket和client。
          //serverSocket仍然继续在监听状态，client则负责接收和发送数据
          //clientAddr是一个传出参数，accept返回时，传出客户端的地址和端口号
          //addr_len是一个传入-传出参数，传入的是调用者提供的缓冲区的clientAddr的长度，以避免缓冲区溢出。
          //传出的是客户端地址结构体的实际长度。
          //出错返回-1
          TextClient = accept(ServerTextSocket, (struct sockaddr*)&clientAddr, (socklen_t*)&addr_len);
          if(TextClient < 0)
          {
               perror("accept");
               continue;
          }
          s_SocketStatus = 1;
          pthread_create(&s_RecvID, NULL, RecvTextPthread, NULL);
          pthread_detach(s_RecvID);
          
          printf("等待消息...\n");
          //inet_ntoa ip地址转换函数，将网络字节序IP转换为点分十进制IP
          //表达式：char *inet_ntoa (struct in_addr);
          printf("IP is %s\n", inet_ntoa(clientAddr.sin_addr));
          printf("Port is %d\n", htons(clientAddr.sin_port));
          while(1)
          {
               TcpStatus = GetTCPTextStatus();
               if(TcpStatus == TCP_ESTABLISHED || TcpStatus == TCP_LISTEN)
               {
                    //Normal
               }
               else
               {
                    printf("Socket1已断开\n");
                    if(s_SocketStatus == 1)
                    {
                         shutdown(TextClient, SHUT_RDWR);
                         pthread_cancel(s_RecvID);
                         s_SocketStatus = 0;
                    }
                    
                    break;
               }
               

               if(s_SocketStatus == 1)
               {
                    // Count++;
                    // memset(buffer, 0 , sizeof(buffer));
                    // sprintf(buffer,"%d",Count);
                    // send(TextClient, buffer, strlen(buffer), 0);
               }
               else
               {
                    printf("Socket已断开\n");
                    shutdown(TextClient, SHUT_RDWR);
                    break;
               }
               
               
               sleep(1);
          }
     }
     close(ServerTextSocket);
}


void *ServerCmdPthread(void* arg)
{
     int TcpStatus = 0;
     struct sockaddr_in server_addr;
     struct sockaddr_in clientAddr;
     int addr_len = sizeof(clientAddr);
     
     char buffer[MAX_DATA];
     int iDataNum;
     static unsigned int Count = 0;
     char  FIleBuf[FILE_MAX];
     FILE *fp;
     int  filelen;
     char strtime[30] = {0}, FileName[40] = {0};

     printf("ServerCmdPthread, Thread ID = %lu\n", pthread_self());
     //socket函数，失败返回-1
     //int socket(int domain, int type, int protocol);
     //第一个参数表示使用的地址类型，一般都是ipv4，AF_INET
     //第二个参数表示套接字类型：tcp：面向连接的稳定数据传输SOCK_STREAM
     //第三个参数设置为0
     if((ServerCmdSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     {
          perror("socket");
          return 0;
     }

     int opt = 1;
     setsockopt(ServerCmdSocket,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof( opt ));
     //socket程序退出后端口依然被占用问题的解决

     bzero(&server_addr, sizeof(server_addr));
     //初始化服务器端的套接字，并用htons和htonl将端口和地址转成网络字节序
     server_addr.sin_family = AF_INET;
     server_addr.sin_port = htons(SERVER_CMD_PORT);
     //ip可是是本服务器的ip，也可以用宏INADDR_ANY代替，代表0.0.0.0，表明所有地址
     server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
     //对于bind，accept之类的函数，里面套接字参数都是需要强制转换成(struct sockaddr *)
     //bind三个参数：服务器端的套接字的文件描述符，
     if(bind(ServerCmdSocket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
     {
          perror("connect");
          close(ServerCmdSocket);
          return 0;
     }
     //设置服务器上的socket为监听状态
     // printf("设置服务器上的socket为监听状态\n");
     if(listen(ServerCmdSocket, 5) < 0)
     {
          perror("listen");
          return 0;
     }

     while(1)
     {
          printf("Cmd指令监听端口: %d\n", SERVER_CMD_PORT);
       
          CmdClient = accept(ServerCmdSocket, (struct sockaddr*)&clientAddr, (socklen_t*)&addr_len);
          if(CmdClient < 0)
          {
               perror("accept");
               continue;
          }
          s_CmdSocketStatus = 1;
          pthread_create(&s_RecvCmdID, NULL, RecvCmdPthread, NULL);
          pthread_detach(s_RecvCmdID);
          
          printf("等待消息...\n");
          //inet_ntoa ip地址转换函数，将网络字节序IP转换为点分十进制IP
          //表达式：char *inet_ntoa (struct in_addr);
          printf("IP is %s\n", inet_ntoa(clientAddr.sin_addr));
          printf("Port is %d\n", htons(clientAddr.sin_port));
          while(1)
          {
               TcpStatus = GetTCPTextStatus();
               if(TcpStatus == TCP_ESTABLISHED || TcpStatus == TCP_LISTEN)
               {
                    //Normal
               }
               else
               {
                    // printf("Socket1已断开\n");
                    if(s_CmdSocketStatus == 1)
                    {
                         shutdown(CmdClient, SHUT_RDWR);
                         pthread_cancel(s_RecvCmdID);
                         s_CmdSocketStatus = 0;
                    }
                    
                    break;
               }
               

               if(s_CmdSocketStatus == 1)
               {

               }
               else
               {
                    printf("Socket已断开\n");
                    close(CmdClient);
                    break;
               }
               
               
               usleep(1000);
          }
     }
     close(ServerCmdSocket);
}

 int WritePhotoList(char *path, char *cmd)	//图片名称追加操作
 {
 	FILE *fp;
 	if((fp = fopen(path, "a+")) == NULL)
 	{
 		perror("fopen() 不能操作挂载文件");
 		return -1;
 	}
 	fwrite(cmd, strlen(cmd), 1, fp);
 	char *next = "\n";
 	fwrite(next, strlen(next), 1, fp);
 	fclose(fp);
 	return 1;
 }

int SetPhotoName(char *cmd)
{
	FILE *fp;

	if((fp = fopen(PHOTONAME_FILE_PATH, "wr")) == NULL)//图片名称
	{
		perror("fopen() 不能操作挂载文件");
		return -1;
	}
	ftruncate(fp,0);
	fprintf(fp, "%s", cmd);
	fclose(fp);
	return 1;
}

void *ServerFilePthread(void* arg)
{
     int TcpStatus = 0;
     struct sockaddr_in server_addr;
     struct sockaddr_in clientAddr;
     int addr_len = sizeof(clientAddr);
     char photolistname[200] = {0};

     char buffer[MAX_DATA];
     int iDataNum;
     static unsigned int Count = 0;
     char  FIleBuf[FILE_MAX];
     FILE *fp;
     int  filelen;
     char strtime[30] = {0}, FileName[100] = {0};

     printf("ServerFilePthread, Thread ID = %lu\n", pthread_self());
     //socket函数，失败返回-1
     //int socket(int domain, int type, int protocol);
     //第一个参数表示使用的地址类型，一般都是ipv4，AF_INET
     //第二个参数表示套接字类型：tcp：面向连接的稳定数据传输SOCK_STREAM
     //第三个参数设置为0
     if((ServerFileSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     {
          perror("socket");
          return 0;
     }

     int opt = 1;
     setsockopt(ServerFileSocket,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof( opt ));
     //socket程序退出后端口依然被占用问题的解决

     bzero(&server_addr, sizeof(server_addr));
     //初始化服务器端的套接字，并用htons和htonl将端口和地址转成网络字节序
     server_addr.sin_family = AF_INET;
     server_addr.sin_port = htons(SERVER_FILE_PORT);
     //ip可是是本服务器的ip，也可以用宏INADDR_ANY代替，代表0.0.0.0，表明所有地址
     server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
     //对于bind，accept之类的函数，里面套接字参数都是需要强制转换成(struct sockaddr *)
     //bind三个参数：服务器端的套接字的文件描述符，
     if(bind(ServerFileSocket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
     {
          perror("connect");
          close(ServerFileSocket);
          return 0;
     }
     //设置服务器上的socket为监听状态
     // printf("设置服务器上的socket为监听状态\n");
     if(listen(ServerFileSocket, 5) < 0)
     {
          perror("listen");
          return 0;
     }

     while (1)
     {
          printf("图片监听端口：%d\n", SERVER_FILE_PORT);
          while(1)
          {
               if( (FileClient = accept(ServerFileSocket, (struct sockaddr*)&clientAddr, &addr_len)) == -1)
               {
                    printf("accept socket error: %s(errno: %d)",strerror(errno),errno);
                    continue;
               }
               GetSystemTimeStr(strtime);
               sprintf(FileName, "/data/website/tcrobot/photos/A_%d_%s_%03d.jpg", s_PhotoIndex, strtime, s_PositionPoint);
               sprintf(photolistname, "A_%d_%s_%03d.jpg", s_PhotoIndex, strtime, s_PositionPoint);
               WritePhotoList(PHOTOLISTPATH, photolistname);
               SetPhotoName(photolistname);
               printf("[%s]建立连接，开始接收图片\n", strtime);
               if((fp = fopen(FileName,"ab") ) == NULL )
               {
                    printf("File.\n");
                    close(ServerFileSocket);
                    exit(1);
               }
               while(1)
               {
                    filelen = read(FileClient, FIleBuf, FILE_MAX);
                    if(filelen == 0)
                         break;
                    fwrite(FIleBuf, 1, filelen, fp);
               }
               FIleBuf[filelen] = '\0';
               GetSystemTimeStr2(strtime);
               printf(YELLOW"[%s]图片接收完成:%s\n"NONE, strtime, FileName);
               fclose(fp);
               close(FileClient);
               // shutdown(FileClient, SHUT_RDWR);
               
          }
     }
}

unsigned int ArrayToInt32(unsigned char *pDst, unsigned char count)
{
	unsigned int Result = 0;
	unsigned char i;
	
	for(i=0; i<count; i++)
	{
		Result = (pDst[i] - '0') + Result*10;
	}
	
	return Result;
}


void *KeyBoardPthread(void* arg)
{
     char strKey[100];
      unsigned  int Ver, Hor;
     printf("KeyBoardPthread, Thread ID = %lu\n", pthread_self());

     while (1)
     {
          memset(strKey, 0, sizeof(strKey));
          scanf("%s", strKey);
          if(strncmp((char *)strKey, "photo", sizeof("photo")) == 0)
          {
               printf("发送当前点拍照指令\n");
               TaskForServerSendCmd(CMD_TAKEAPHOTO);
          }
          else if(strncmp((char *)strKey, "go", 2) == 0)
          {
               s_Angle_X = ArrayToInt32(&strKey[3], 3);
               s_Angle_Y = ArrayToInt32(&strKey[7], 2);
               printf("发送定点拍照指令:X:%d:Y:%d\n", s_Angle_X, s_Angle_Y);     
               TaskForServerSendCmd(CMD_GOTOTAKEAPHOTO);
          }
          else if(strncmp((char *)strKey, "xy", 2) == 0)
          {
               s_Angle_X = ArrayToInt32(&strKey[3], 3);
               s_Angle_Y = ArrayToInt32(&strKey[7], 2);
               printf("发送云台旋转指令:X:%d :Y:%d\n", s_Angle_X, s_Angle_Y);
               
               TaskForServerSendCmd(CMD_GOTOANGLE);
          }
          else if(strncmp((char *)strKey, "ret", sizeof("ret")) == 0)
          {
               printf("发送返回原点指令\n");
               TaskForServerSendCmd(CMD_RETURN);
          }
          else if(strncmp((char *)strKey, "get", sizeof("get")) == 0)
          {
               printf("发送获取参数指令\n");
               TaskForServerSendCmd(CMD_GETPARM);
          }
          else if(strncmp((char *)strKey, "set", sizeof("set")) == 0)
          {
               printf("发送设置拍照参数指令\n");
               TaskForServerSendCmd(CMD_SETPARM);
          }
          else if(strncmp((char *)strKey, "reboot", sizeof("rst")) == 0)
          {
               printf("发送重启指令\n");
               TaskForServerSendCmd(CMD_REBOOT);
          }
          usleep(10000);
     }
     
}

static void OnRxMatchcJsonMsg(JSON_COMM_MSG *cJsonMsg)
{
	if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_TAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_TAKEAPHOTO)	
	{
		// printf("收到应答,发送成功，Kill the CMD_TAKEAPHOTO\r\n");
		pthread_cancel(s_TakeaPhotoID);
          cJsonMsgInfo[CMD_TAKEAPHOTO].Ack = RECACK;
          cJsonMsgInfo[CMD_TAKEAPHOTO].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_GOTOTAKEAPHOTO)	
	{
		// printf("收到应答,发送成功，Kill the CMD_GOTOTAKEAPHOTO\r\n");
		pthread_cancel(s_GotoTakeaPhotoID);
          cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack = RECACK;
          cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOANGLE].Seq) && cJsonMsg->CMD == CMD_GOTOANGLE)	
	{
		// printf("收到应答,发送成功，Kill the CMD_GOTOANGLE\r\n");
		pthread_cancel(s_GotoAngleID);
          cJsonMsgInfo[CMD_GOTOANGLE].Ack = RECACK;
          cJsonMsgInfo[CMD_GOTOANGLE].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_RETURN].Seq) && cJsonMsg->CMD == CMD_RETURN)	
	{
		// printf("收到应答,发送成功，Kill the CMD_RETURN\r\n");
		pthread_cancel(s_ReturnID);
          cJsonMsgInfo[CMD_RETURN].Ack = RECACK;
          cJsonMsgInfo[CMD_RETURN].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GETPARM].Seq) && cJsonMsg->CMD == CMD_GETPARM)	
	{
		// printf("收到应答,发送成功，Kill the CMD_GETPARM\r\n");
		pthread_cancel(s_GetParmID);
          cJsonMsgInfo[CMD_GETPARM].Ack = RECACK;
          cJsonMsgInfo[CMD_GETPARM].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_SETPARM].Seq) && cJsonMsg->CMD == CMD_SETPARM)	
	{
		// printf("收到应答,发送成功，Kill the CMD_SETPARM\r\n");
		pthread_cancel(s_SetParmID);
          cJsonMsgInfo[CMD_SETPARM].Ack = RECACK;
          cJsonMsgInfo[CMD_SETPARM].isWorking = SEND_IDLE;
	}
}

void *RecvTextPthread(void *arg)
{
     static unsigned int RecCount = 0;
     char recvbuf[MAX_DATA];
	printf("RecvTextPthread, Thread ID = %lu\n", pthread_self());

     recvbuf[0] = '\0';
     int iDataNum = 0;

     while(1)
     {
          if(s_SocketStatus == 1)
          {
               iDataNum = recv(TextClient, recvbuf, MAX_DATA, MSG_DONTWAIT);
               if(iDataNum > 0 && iDataNum < MAX_DATA)
               {
                    RecCount = 0;
                    recvbuf[iDataNum] = '\0';
                    printf("Rec----:%s\n", recvbuf);
                    DecodecJsonMsg(recvbuf);                  
                    memset(recvbuf, 0, sizeof(recvbuf));
               }
               else if(iDataNum == 0)
               {
                    printf("Socket断开:%d\n", iDataNum);
                    shutdown(TextClient, SHUT_RDWR);
                    s_SocketStatus = 0;
                    RecCount = 0;
                    pthread_exit(NULL);
               }
          }
          
          RecCount++;
          if(RecCount >= 1000)
          {
               printf("超时未收到心跳包，断开连接\n");
               shutdown(TextClient, SHUT_RDWR);
               RecCount = 0;
               s_SocketStatus = 0;
               pthread_exit(NULL);
          }
          // printf("RecCount:%d\n", RecCount);
          usleep(10000);
     }
     
}

void *RecvCmdPthread(void *arg)
{
     static unsigned int RecCount = 0;
     char recvbuf[MAX_DATA];
	printf("RecvCmdPthread, Thread ID = %lu\n", pthread_self());

     recvbuf[0] = '\0';
     int iDataNum = 0;

     while(1)
     {
          if(s_CmdSocketStatus == 1)
          {
               iDataNum = recv(CmdClient, recvbuf, MAX_DATA, MSG_DONTWAIT);
               if(iDataNum > 0 && iDataNum < MAX_DATA)
               {
                    RecCount = 0;
                    recvbuf[iDataNum] = '\0';
                    printf("Rec Cmd----:%s\n", recvbuf);
                    DecodecJsonCmd(recvbuf);                  
                    memset(recvbuf, 0, sizeof(recvbuf));
                    // shutdown(CmdClient, SHUT_RDWR);
                    // s_CmdSocketStatus = 0;
                    // RecCount = 0;
                    // pthread_exit(NULL);
               }
               else if(iDataNum == 0)
               {
                    // printf("Socket断开:%d\n", iDataNum);
                    shutdown(CmdClient, SHUT_RDWR);
                    s_CmdSocketStatus = 0;
                    RecCount = 0;
                    pthread_exit(NULL);
               }
          }
          
          // RecCount++;
          // if(RecCount >= 120)
          // {
          //      printf("超时未收到Cmd,断开连接\n");
          //      shutdown(CmdClient, SHUT_RDWR);
          //      RecCount = 0;
          //      s_CmdSocketStatus = 0;
          //      pthread_exit(NULL);
          // }
          // printf("RecCount:%d\n", RecCount);
          usleep(10000);
     }
     
}
//发送拍照指令
void *TakeaPhotoPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("TakeaPhotoPthread, Thread ID = %lu\n", s_TakeaPhotoID);
     cJsonMsgInfo[CMD_TAKEAPHOTO].CMD = CMD_TAKEAPHOTO;
	cJsonMsgInfo[CMD_TAKEAPHOTO].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_TAKEAPHOTO].Seq = s_Seq++;
     cJsonMsgInfo[CMD_TAKEAPHOTO].Ack = NOACK;
     cJsonMsgInfo[CMD_TAKEAPHOTO].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_TAKEAPHOTO].Ack == NOACK)
               ServertoClientSendCMd(CMD_TAKEAPHOTO, cJsonMsgInfo[CMD_TAKEAPHOTO].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_TAKEAPHOTO].Ack == RECACK)
          {
               printf("TakeaPhoto发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_TAKEAPHOTO].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//发送定点拍照指令
void *GotoTakeaPhotoPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("GotoTakeaPhotoPthread, Thread ID = %lu\n", s_GotoTakeaPhotoID);
     cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].CMD = CMD_GOTOTAKEAPHOTO;
	cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Seq = s_Seq++;
     cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack = NOACK;
     cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack == NOACK)
               ServertoClientSendCMd(CMD_GOTOTAKEAPHOTO, cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack == RECACK)
          {
               printf("GotoTakeaPhoto发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//转动到指定角度
void *GotoAnglePthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("TakeaPhotoPthread, Thread ID = %lu\n", s_GotoAngleID);
     cJsonMsgInfo[CMD_GOTOANGLE].CMD = CMD_GOTOANGLE;
	cJsonMsgInfo[CMD_GOTOANGLE].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_GOTOANGLE].Seq = s_Seq++;
     cJsonMsgInfo[CMD_GOTOANGLE].Ack = NOACK;
     cJsonMsgInfo[CMD_GOTOANGLE].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_GOTOANGLE].Ack == NOACK)
               ServertoClientSendCMd(CMD_GOTOANGLE, cJsonMsgInfo[CMD_GOTOANGLE].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_GOTOANGLE].Ack == RECACK)
          {
               printf("GotoAngle发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }         

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_GOTOANGLE].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//云台返回原点指令
void *ReturnPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("ReturnPthread, Thread ID = %lu\n", s_ReturnID);
     cJsonMsgInfo[CMD_RETURN].CMD = CMD_RETURN;
	cJsonMsgInfo[CMD_RETURN].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_RETURN].Seq = s_Seq++;
     cJsonMsgInfo[CMD_RETURN].Ack = NOACK;
     cJsonMsgInfo[CMD_RETURN].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_RETURN].Ack == NOACK)
               ServertoClientSendCMd(CMD_RETURN, cJsonMsgInfo[CMD_RETURN].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_RETURN].Ack == RECACK)
          {
               printf("Return发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_RETURN].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//获取相机拍照参数指令
void *GetParmPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("GetParmIDPthread, Thread ID = %lu\n", s_GetParmID);
     cJsonMsgInfo[CMD_GETPARM].CMD = CMD_GETPARM;
	cJsonMsgInfo[CMD_GETPARM].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_GETPARM].Seq = s_Seq++;
     cJsonMsgInfo[CMD_GETPARM].Ack = NOACK;
     cJsonMsgInfo[CMD_GETPARM].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_GETPARM].Ack == NOACK)
               ServertoClientSendCMd(CMD_GETPARM, cJsonMsgInfo[CMD_GETPARM].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_GETPARM].Ack == RECACK)
          {
               printf("GetParm发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_GETPARM].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//设置相机拍照参数指令
void *SetParmPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("SetParmPthread, Thread ID = %lu\n", s_SetParmID);
     cJsonMsgInfo[CMD_SETPARM].CMD = CMD_SETPARM;
	cJsonMsgInfo[CMD_SETPARM].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_SETPARM].Seq = s_Seq++;
     cJsonMsgInfo[CMD_SETPARM].Ack = NOACK;
     cJsonMsgInfo[CMD_SETPARM].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_SETPARM].Ack == NOACK)
               ServertoClientSendCMd(CMD_SETPARM, cJsonMsgInfo[CMD_SETPARM].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_SETPARM].Ack == RECACK)
          {
               printf("SetParm发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_SETPARM].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
//重启指令
void *RebootPthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("SetParmPthread, Thread ID = %lu\n", s_RebootID);
     cJsonMsgInfo[CMD_REBOOT].CMD = CMD_REBOOT;
	cJsonMsgInfo[CMD_REBOOT].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_REBOOT].Seq = s_Seq++;
     cJsonMsgInfo[CMD_REBOOT].Ack = NOACK;
     cJsonMsgInfo[CMD_REBOOT].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_REBOOT].Ack == NOACK)
               ServertoClientSendCMd(CMD_REBOOT, cJsonMsgInfo[CMD_REBOOT].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_REBOOT].Ack == RECACK)
          {
               printf("Reboot发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_SETPARM].isWorking = SEND_IDLE;
     }

     pthread_exit(NULL);   
}
void TaskForServerSendCmd(unsigned char Cmd)
{
     if(s_SocketStatus == 0)
     {
          printf("TCP未连接\n");
          return;
     }
     switch (Cmd)
     {
          case CMD_TAKEAPHOTO:
               if(cJsonMsgInfo[CMD_TAKEAPHOTO].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_TakeaPhotoID, NULL, TakeaPhotoPthread, NULL);
                    pthread_detach(s_TakeaPhotoID);
               }
          break;
          
          case CMD_GOTOTAKEAPHOTO:
               if(cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_GotoTakeaPhotoID, NULL, GotoTakeaPhotoPthread, NULL);
                    pthread_detach(s_GotoTakeaPhotoID);
               }
          break;

          case CMD_GOTOANGLE:
               if(cJsonMsgInfo[CMD_GOTOANGLE].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_GotoAngleID, NULL, GotoAnglePthread, NULL);
                    pthread_detach(s_GotoAngleID);
               }
          break;

          case CMD_RETURN:
               if(cJsonMsgInfo[CMD_RETURN].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_ReturnID, NULL, ReturnPthread, NULL);
                    pthread_detach(s_ReturnID);
               }
          break;

          case CMD_GETPARM:
               if(cJsonMsgInfo[CMD_GETPARM].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_GetParmID, NULL, GetParmPthread, NULL);
                    pthread_detach(s_GetParmID);
               }
          break;

          case CMD_SETPARM:
               if(cJsonMsgInfo[CMD_SETPARM].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_SetParmID, NULL, SetParmPthread, NULL);
                    pthread_detach(s_SetParmID);
               }
          break;

        case CMD_REBOOT:
               if(cJsonMsgInfo[CMD_SETPARM].isWorking == SEND_IDLE)
               {
                    pthread_create(&s_SetParmID, NULL, RebootPthread, NULL);
                    pthread_detach(s_SetParmID);
               }
          break;

          default:

          break;
     }
     
}

void ServertoClientSendCMd(unsigned char Cmd, unsigned int Seq)  // 客户端想服务的发送心跳包
{
     int ret = 0, val = 0;
     char *cJsonBuf;
     unsigned int ParmCount = 0;
     char strTime[30];
     cJSON *Root, *cjson_sub;
     unsigned short MsgLen = 0;
     char strtime[30] = {0};

     GetSystemTimeStr2(strtime);
     Root = cJSON_CreateObject();
     cjson_sub = cJSON_CreateObject();
     cJSON_AddNumberToObject(Root,"CMD",		 Cmd);
     cJSON_AddNumberToObject(Root,"Dir",		 CMD_MASTER_SEND);
     cJSON_AddNumberToObject(Root,"Seq",		 Seq);
     cJSON_AddStringToObject(Root,"Time",		 strtime);
     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

     if(Cmd == CMD_GOTOTAKEAPHOTO || Cmd == CMD_GOTOANGLE) //指定点拍照
     {
          cJSON_AddNumberToObject(cjson_sub, "X", s_Angle_X);

          cJSON_AddNumberToObject(cjson_sub, "Y", s_Angle_Y);

          cJSON_AddNumberToObject(cjson_sub, "Ponit", s_PositionPoint);
     }
     else if(Cmd == CMD_SETPARM)
     {
          if(ReadConfigIni("DRC","DRC_EN",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"DRC_EN = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "DRC_EN", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("DRC","WhiteLevel",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"WhiteLevel = %d\n"NONE, val);  
               cJSON_AddNumberToObject(cjson_sub, "WhiteLevel", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("DRC","BrightPr",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"BrightPr = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "BrightPr", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("AE","ExpTimeRangeMin",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"ExpTimeRangeMin = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "ExpTimeRangeMin", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("AE","AEMaxGainRange",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"AEMaxGainRange = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "AEMaxGainRange", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("AE","Compensation",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"Compensation = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "Compensation", val);
               ParmCount++;
          }
               
          if(ReadConfigIni("CSC","LumaVal",&val)==READ_CONFIG_OK)
          {
               printf(L_CYAN"LumaVal = %d\n"NONE, val);
               cJSON_AddNumberToObject(cjson_sub, "LumaVal", val);
               ParmCount++;
          }

          if(GetProfileString(CONFIG_FILE, "Device_IP", "eth0ip", s_eth0IP)==READ_CONFIG_OK)
          {
               printf(L_CYAN"eth0ip = %s\n"NONE, s_eth0IP);
               cJSON_AddStringToObject(cjson_sub, "ServerIP", s_eth0IP);
               ParmCount++;
          }

          if(ParmCount < 7)
          {
               printf("读取参数出错\n");
               return;
          }
     }

     cJsonBuf = cJSON_PrintUnformatted(Root);

     MsgLen = strlen(cJsonBuf);

     cJSON_Delete(Root);

     strcat(cJsonBuf, "\r");

     ret = send(TextClient, cJsonBuf, MsgLen, MSG_NOSIGNAL);

     if(ret < 0)
     {
          printf("Socket 连接已经断开\n");
          shutdown(TextClient, SHUT_RDWR);
          s_SocketStatus = 0;
     }

     printf("\nClientToServerAlive(%d)(Size:%d):%s\n", ret, MsgLen, cJsonBuf);

     free(cJsonBuf);
}
//发送指令应答包
void ServerToClientAck(cJSON *Root, int Port)  // 客户端想服务的发送心跳包
{
     char *cJsonBuf;
     cJSON *Temp, *cjson_sub;
     unsigned short MsgLen = 0;
     int Dir = 0;

     cJsonBuf = cJSON_PrintUnformatted(Root);

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == 0)
          {
               cJSON_ReplaceItemInObject(Root, "Dir", cJSON_CreateNumber(1));

               cJsonBuf = cJSON_PrintUnformatted(Root);

               MsgLen = strlen(cJsonBuf);

               cJSON_Delete(Root);

               strcat(cJsonBuf, "\r");

               send(Port, cJsonBuf, MsgLen, MSG_NOSIGNAL);

               // printf("Ack(Port:%d):%s\n", Port, cJsonBuf);

               free(cJsonBuf);
          }
     }
     else
     {
          printf("没有Dir\n");
          return ;
     }
}
 //解析CJSON协议包
int DecodecJsonMsg(char *cJsonBuf)
{
     cJSON *Root, *cJsonAck, *CmdAck, *Temp, *Child, *tNode;
     cJSON *cjson_sub = NULL;
     cJSON *item =NULL;
     int ret = 0;
     char *Output, Err;
     JSON_COMM_MSG cJsonMsg;

     unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
     unsigned int Seq = 0;
     unsigned int i;

     if(strlen((const char *)cJsonBuf) >= 1024)
     {
          printf(" Data Length Over\n");
          return -1;
     }
     Root = cJSON_Parse((const char *)cJsonBuf);
     cJsonAck = cJSON_Parse((const char *)cJsonBuf);
     CmdAck = cJSON_Parse((const char *)cJsonBuf);
     // printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
     if (Root == NULL )
     {
     //  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "CMD");
     if(Temp != NULL)
     {
          Cmd = Temp->valueint;
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == CMD_MASTER_SEND)
          {
               ServerToClientAck(cJsonAck, TextClient);
          }
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Seq");
     if(Temp != NULL)
     {
          Seq = Temp->valueint;
     }
     else
     {
          return -1;
     }

     if(Dir == CMD_MASTER_SEND)
     {

     } 
     else
     {
          cJsonMsg.CMD = Cmd;
          cJsonMsg.Dir = Dir;
          cJsonMsg.Seq = Seq;
          OnRxMatchcJsonMsg(&cJsonMsg);
     }

     Child = cJSON_GetObjectItem(Root, "Msg");
     if((Child != NULL) && (Child->type == cJSON_Object))
     {
          tNode = cJSON_GetObjectItem(Child, "Status");
          if(tNode != NULL)
          {
          
               if(strncmp((char *)tNode->valuestring, "OK", 2) == 0)	
               {
                    printf("转发给接口App\n");
                    ServerToClientAck(CmdAck, CmdClient);
               }
               else if(strncmp((char *)tNode->valuestring, "Ang", 3) == 0)	
               {

               }
          }
               
     }
          Output = cJSON_PrintUnformatted(Root);
          cJSON_Delete(Root);
          free(Output);

     return ret;
}

 //解析CJSON协议包
int DecodecJsonCmd(char *cJsonBuf)
{
     cJSON *Root, *cJsonAck, *Temp, *Child, *tNode;
     cJSON *cjson_sub = NULL;
     cJSON *item =NULL;
     int ret = 0;
     char *Output, Err;
     JSON_COMM_MSG cJsonMsg;
     char strtime[50] = {0};
     unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
     unsigned int Seq = 0;
     unsigned int i;

     if(strlen((const char *)cJsonBuf) >= 1024)
     {
          printf(" Data Length Over\n");
          return -1;
     }
     GetSystemTimeStr(strtime);
     Root = cJSON_Parse((const char *)cJsonBuf);
     cJsonAck = cJSON_Parse((const char *)cJsonBuf);
     // printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
     if (Root == NULL )
     {
     //  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "CMD");
     if(Temp != NULL)
     {
          Cmd = Temp->valueint;
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == CMD_MASTER_SEND)
          {
               ServerToClientAck(cJsonAck, CmdClient);
          }
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Seq");
     if(Temp != NULL)
     {
          Seq = Temp->valueint;
     }
     else
     {
          return -1;
     }

   
     Child = cJSON_GetObjectItem(Root, "Msg");
     if((Child != NULL) && (Child->type == cJSON_Object))
     {
          tNode = cJSON_GetObjectItem(Child, "X");
          if(tNode != NULL)
          {
               s_Angle_X = tNode->valueint;
          }
          else
          {
               s_Angle_X = 0;
          }
          

          tNode = cJSON_GetObjectItem(Child, "Y");
          if(tNode != NULL)
          {
               s_Angle_Y = tNode->valueint;
          }
          else
          {
               s_Angle_Y = 0;
          }

          tNode = cJSON_GetObjectItem(Child, "Index");
          if(tNode != NULL)
          {
               s_PhotoIndex = tNode->valueint;
          }
          else
          {
               s_PhotoIndex = 0;
          }

          tNode = cJSON_GetObjectItem(Child, "Point");
          if(tNode != NULL)
          {
               s_PositionPoint = tNode->valueint;
          }
          else
          {
               s_PositionPoint = 0;
          }

          tNode = cJSON_GetObjectItem(Child, "DRC_EN");
          if(tNode != NULL)
          {
               s_DRC_EN= tNode->valueint;
          }
          else
          {
               s_DRC_EN = 0;
          }
          

          tNode = cJSON_GetObjectItem(Child, "WhiteLevel");
          if(tNode != NULL)
          {
               s_WhiteLevel = tNode->valueint;
          }
          else
          {
               s_WhiteLevel = 800000;
          }
          

          tNode = cJSON_GetObjectItem(Child, "BrightPr");
          if(tNode != NULL)
          {
               s_BrightPr = tNode->valueint;
          }
          else
          {
               s_BrightPr = 200; 
          }



        tNode = cJSON_GetObjectItem(Child, "ExpTimeRangeMin");
          if(tNode != NULL)
          {
               s_ExpTimeRangeMin = tNode->valueint;
          }
          else
          {
               s_ExpTimeRangeMin = 1000;
          }
          

          tNode = cJSON_GetObjectItem(Child, "Compensation");
          if(tNode != NULL)
          {
               s_Compensation = tNode->valueint;
          }
          else
          {
               s_Compensation = 60;
          }
          

          tNode = cJSON_GetObjectItem(Child, "AEMaxGainRange");
          if(tNode != NULL)
          {
               s_AEMaxGainRange = tNode->valueint;
          }
          else
          {
               s_AEMaxGainRange = 0;
          }
          

          tNode = cJSON_GetObjectItem(Child, "LumaVal");
          if(tNode != NULL)
          {
               s_LumaVal = tNode->valueint;
          }
          else
          {
               s_LumaVal = 0;
          }
               
     }


     TaskForServerSendCmd(Cmd);
   

     Output = cJSON_PrintUnformatted(Root);
     cJSON_Delete(Root);
     free(Output);

     return ret;
}

int GetTCPTextStatus(void)
{
     struct tcp_info info;
     int len = sizeof(info);

     if(ServerTextSocket>0)
     {
          getsockopt(ServerTextSocket, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
          
          return info.tcpi_state;

     }
}

int GetTCPFileStatus(void)
{
     struct tcp_info info;
     int len = sizeof(info);

     if(ServerFileSocket>0)
     {
          getsockopt(ServerFileSocket, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
          
          return info.tcpi_state;

     }
}


/*
监听后，一直处于accept阻塞状态，
直到有客户端连接，
当客户端如数quit后，断开与客户端的连接
*/
int main()
{
     //调用socket函数返回的文件描述符

     //声明两个套接字sockaddr_in结构体变量，分别表示客户端和服务器
    char strtime[30];
     
	
     signal(SIGINT, sig_handler);
     signal(SIGTERM, sig_handler);

     pthread_create(&s_KeyboardID, NULL, KeyBoardPthread, NULL);
     pthread_create(&s_ServerTextID, NULL, ServerTextPthread, NULL);
     pthread_create(&s_ServerFileID, NULL, ServerFilePthread, NULL);
     pthread_create(&s_ServerCmdID, NULL, ServerCmdPthread, NULL);

     while (1)
     {
          sleep(10);
          GetSystemTimeStr2(strtime);
          printf("[%s]Running...\n", strtime);
     }
     return 0;
}