#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "cjson.h"
#include <pthread.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <net/if.h>

#define   SERVER_TEXT_PORT 6699    //文本通信端口
#define   SERVER_FILE_PORT 6698    //文件收发端口
#define   SERVER_CMD_PORT 6697     //内部指令端口

#define   FILE_MAX  102400
#define   MAX_DATA  200

#define        SERVER_IP                "172.16.202.60"

#define        CMD_MASTER_SEND               0
#define        CMD_SLAVE_ACK                 1
#define        NOACK                         0
#define        RECACK                        1

#define        SEND_IDLE                     0
#define        SEND_BUSY                     1

#define		CMD_ALIVE				     0
#define		CMD_TAKEAPHOTO			     1
#define		CMD_GOTOTAKEAPHOTO		     2
#define		CMD_GOTOANGLE	               3
#define		CMD_RETURN		          4
#define		CMD_GETPARM			     5	
#define		CMD_SETPARM			     6	
#define		CMD_REBOOT			     7	
//颜色定义
#define NONE                 "\e[0m"
#define BLACK                "\e[0;30m"
#define L_BLACK              "\e[1;30m"
#define RED                  "\e[0;31m"
#define L_RED                "\e[1;31m"
#define GREEN                "\e[0;32m"
#define L_GREEN              "\e[1;32m"
#define BROWN                "\e[0;33m"
#define YELLOW               "\e[1;33m"
#define BLUE                 "\e[0;34m"
#define L_BLUE               "\e[1;34m"
#define PURPLE               "\e[0;35m"     //紫色
#define L_PURPLE             "\e[1;35m"
#define CYAN                 "\e[0;36m"     //天蓝色
#define L_CYAN               "\e[1;36m"
#define GRAY                 "\e[0;37m"     //灰色
#define WHITE                "\e[1;37m"

typedef enum 
{
	ALIVE_NUM           = 0, 
	TAKEAPHOTO_NUM      = 1,
	GOTOTAKEAPHOTO_NUM  = 2,
	GOTOANGLE_NUM       = 3,
	RETURN_NUM		= 4,
     GETPARM_NUM         = 5,
     SETPARM_NUM         = 6,
     CJSON_NUM_MAX       
} 
CJSON_STRUCT;

typedef struct
{
	unsigned char 	CMD;
	unsigned char 	Dir;
	unsigned int 	Seq;
     unsigned char  Ack;
     unsigned char  isWorking;
}JSON_COMM_MSG; 

static JSON_COMM_MSG	cJsonMsgInfo[CJSON_NUM_MAX];		//JSON信息存储，用于重发、应答
static pthread_t s_RecvID, s_AliveID, s_TakeaPhotoID, s_GotoTakeaPhotoID, s_GotoAngleID, s_ReturnID, s_GetParmID, s_SetParmID;
int clientSocket;
int DecodecJsonMsg(char *cJsonBuf);
static unsigned char s_SocketStatus = 0, s_AliveAck = 0;
void ClientToServerAlive(unsigned int Seq);
static unsigned s_CmdNow = 0;//当前需要下发的指令
static int s_Angle_X = 0, s_Angle_Y = 0, s_PhotoIndex, s_PositionPoint;

void sig_handler(int signum)
{
     printf("client线程退出\n");
     pthread_cancel(s_RecvID);
     close(clientSocket);
     exit(1);
}

static void OnRxMatchcJsonMsg(JSON_COMM_MSG *cJsonMsg)
{
	if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_ALIVE].Seq) && cJsonMsg->CMD == CMD_ALIVE )	
	{
		// printf(" 发送成功，Rec Ack, Kill the Alive\r\n");
		// pthread_cancel(s_AliveID);
          cJsonMsgInfo[CMD_ALIVE].Ack = RECACK;
          cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_TAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_TAKEAPHOTO)	
	{
		// printf(" Rec Ack, Kill the CMD_TAKEAPHOTO\r\n");
		// pthread_detach(s_TakeaPhotoID);
          cJsonMsgInfo[CMD_TAKEAPHOTO].Ack = RECACK;
          cJsonMsgInfo[CMD_TAKEAPHOTO].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Seq) && cJsonMsg->CMD == CMD_GOTOTAKEAPHOTO)	
	{
		// printf(" Rec Ack, Kill the CMD_GOTOTAKEAPHOTO\r\n");
		// pthread_detach(s_GotoTakeaPhotoID);
           cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].Ack = RECACK;
           cJsonMsgInfo[CMD_GOTOTAKEAPHOTO].isWorking = SEND_IDLE;
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GOTOANGLE].Seq) && cJsonMsg->CMD == CMD_GOTOANGLE)	
	{
		// printf(" Rec Ack, Kill the CMD_GOTOANGLE\r\n");
		// pthread_detach(s_GotoAngleID);
           cJsonMsgInfo[CMD_GOTOANGLE].Ack = RECACK;
           cJsonMsgInfo[CMD_GOTOANGLE].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_RETURN].Seq) && cJsonMsg->CMD == CMD_RETURN)	
	{
		// printf(" Rec Ack, Kill the CMD_RETURN\r\n");
		// pthread_detach(s_ReturnID);
           cJsonMsgInfo[CMD_RETURN].Ack = RECACK;
           cJsonMsgInfo[CMD_RETURN].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_GETPARM].Seq) && cJsonMsg->CMD == CMD_GETPARM)	
	{
		// printf(" Rec Ack, Kill the CMD_GETPARM\r\n");
		// pthread_detach(s_GetParmID);
           cJsonMsgInfo[CMD_GETPARM].Ack = RECACK;
           cJsonMsgInfo[CMD_GETPARM].isWorking = SEND_IDLE;
	}
     else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[CMD_SETPARM].Seq) && cJsonMsg->CMD == CMD_SETPARM)	
	{
		// printf(" Rec Ack, Kill the CMD_SETPARM\r\n");
		// pthread_detach(s_SetParmID);
           cJsonMsgInfo[CMD_SETPARM].Ack = RECACK;
           cJsonMsgInfo[CMD_SETPARM].isWorking = SEND_IDLE;
	}
}

void *RecvPthread(void *arg)
{
     char recvbuf[MAX_DATA];
     printf("RecvPthread, Thread ID = %lu\n", s_RecvID);

     recvbuf[0] = '\0';
     int iDataNum;

     while(1)
     {
          iDataNum = recv(clientSocket, recvbuf, MAX_DATA, MSG_DONTWAIT); 
          if(iDataNum > 0)
          {
               recvbuf[iDataNum] = '\0';
               printf("Rec%s\n", recvbuf);
               DecodecJsonMsg(recvbuf);
               memset(recvbuf, 0, sizeof(recvbuf));
               s_AliveAck = 1;
          }
          else if(iDataNum == 0)
          {
               // printf("Socket断开:%d\n", iDataNum);
               s_SocketStatus = 0;
          }
          usleep(10);
     }
     
	return NULL;
}

void *KeepAlivePthread(void *arg)
{
     static unsigned int s_Seq = 0;
     static unsigned s_SendCount = 0;
     unsigned int WaitTime = 0;

	printf("KeepAlivePthread, Thread ID = %lu\n", s_AliveID);
     cJsonMsgInfo[CMD_ALIVE].CMD = CMD_ALIVE;
	cJsonMsgInfo[CMD_ALIVE].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[CMD_ALIVE].Seq = s_Seq++;
     cJsonMsgInfo[CMD_ALIVE].Ack = NOACK;
     cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_BUSY;
     s_SendCount = 0;

     while(s_SendCount <= 3)
     {
          if(cJsonMsgInfo[CMD_ALIVE].Ack == NOACK)
               ClientToServerAlive(cJsonMsgInfo[CMD_ALIVE].Seq);
          s_SendCount++;
          sleep(1);
          if(cJsonMsgInfo[CMD_ALIVE].Ack == RECACK)
          {
               printf("发送成功\n");
               pthread_exit(NULL);
               return 0;
          }   
     }

     if(s_SendCount > 3)
     {
          s_SendCount = 0;
          printf("发送失败\n");
          cJsonMsgInfo[CMD_ALIVE].isWorking = SEND_IDLE;
          s_SocketStatus = 0;
          shutdown(clientSocket, SHUT_RDWR);
     }

     pthread_exit(NULL);
     
}

void TaskForKeepAlive(void)
{
     if(cJsonMsgInfo[CMD_ALIVE].isWorking == SEND_IDLE)
     {
          pthread_create(&s_AliveID, NULL, KeepAlivePthread, NULL);
          pthread_detach(s_AliveID);
     }
}

void ClientToServerAlive(unsigned int Seq)  // 客户端想服务的发送心跳包
{
     int ret = 0;
     char *cJsonBuf;
     char strTime[30];
     cJSON *Root, *cjson_sub;
     unsigned short MsgLen = 0;

     Root = cJSON_CreateObject();
     cjson_sub = cJSON_CreateObject();
     cJSON_AddNumberToObject(Root,"CMD",		 CMD_ALIVE);
     cJSON_AddNumberToObject(Root,"Dir",		 CMD_MASTER_SEND);
     cJSON_AddNumberToObject(Root,"Seq",		 Seq);
     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

     cJSON_AddStringToObject(cjson_sub,"Status", 	 "OK");

     cJsonBuf = cJSON_PrintUnformatted(Root);

     MsgLen = strlen(cJsonBuf);

     cJSON_Delete(Root);

     strcat(cJsonBuf, "\r");

     ret = send(clientSocket, cJsonBuf, MsgLen, MSG_NOSIGNAL);

     if(ret < 0)
     {
          printf("Socket 连接已经断开\n");
          shutdown(clientSocket, SHUT_RDWR);
          s_SocketStatus = 0;
     }

     printf("\nClientToServerAlive(%d)(Size:%d):%s\n", ret, MsgLen, cJsonBuf);

     free(cJsonBuf);
}
//发送指令应答包
void ClienttoServerAck(cJSON *Root)  
{
     char *cJsonBuf;
     cJSON *Temp, *cjson_sub;
     unsigned short MsgLen = 0;
     int Dir = 0;

     cJsonBuf = cJSON_PrintUnformatted(Root);

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == 0)
          {
               cJSON_ReplaceItemInObject(Root, "Dir", cJSON_CreateNumber(1));

               cJsonBuf = cJSON_PrintUnformatted(Root);

               MsgLen = strlen(cJsonBuf);

               cJSON_Delete(Root);

               strcat(cJsonBuf, "\r");

               send(clientSocket, cJsonBuf, MsgLen, MSG_NOSIGNAL);

               printf("Ack(Size:%d):%s\n", MsgLen, cJsonBuf);

               free(cJsonBuf);
          }
     }
     else
     {
          printf("没有Dir\n");
          return ;
     }
}
 //解析CJSON协议包
int DecodecJsonMsg(char *cJsonBuf)
{
     cJSON *Root, *cJsonAck, *Temp, *Child, *tNode;
     cJSON *cjson_sub = NULL;
     cJSON *item =NULL;
     int ret = 0;
     char *Output, Err;
     JSON_COMM_MSG cJsonMsg;

     unsigned char Cmd = 0, Dir = 0, HttpLength = 0;
     unsigned int Seq = 0;
     unsigned int i;

     if(strlen((const char *)cJsonBuf) >= 1024)
     {
          printf(" Data Length Over\n");
          return -1;
     }
     Root = cJSON_Parse((const char *)cJsonBuf);
     cJsonAck = cJSON_Parse((const char *)cJsonBuf);
     // printf("\n\nRecv cJson Packs(%ld):%s\n", strlen((const char *)cJsonBuf) - 2, cJsonBuf);
     if (Root == NULL )
     {
     //  printf(" No cJson[%s]\n", cJSON_GetErrorPtr());
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "CMD");
     if(Temp != NULL)
     {
          Cmd = Temp->valueint;
     }
     else
     {
          return -1;
     }

     Temp = cJSON_GetObjectItem(Root, "Dir");
     if(Temp != NULL)
     {
          Dir = Temp->valueint;
          if(Dir == CMD_MASTER_SEND)
          {
               ClienttoServerAck(cJsonAck);
          }
     }
     else
     {
          return -1;
     }
     Temp = cJSON_GetObjectItem(Root, "Seq");
     if(Temp != NULL)
     {
          Seq = Temp->valueint;
     }
     else
     {
          return -1;
     }

     if(Dir == CMD_MASTER_SEND)
     {

     } 
     else
     {
          cJsonMsg.CMD = Cmd;
          cJsonMsg.Dir = Dir;
          cJsonMsg.Seq = Seq;
          OnRxMatchcJsonMsg(&cJsonMsg);
     }
     

     Child = cJSON_GetObjectItem(Root, "Msg");
     if((Child != NULL) && (Child->type == cJSON_Object))
     {
          tNode = cJSON_GetObjectItem(Child, "Status");
          if(tNode != NULL)
          {
          
               if(strncmp((char *)tNode->valuestring, "OK", 2) == 0)	
               {

               }
               else if(strncmp((char *)tNode->valuestring, "Ang", 3) == 0)	
               {

               }
          }
               
     }
          Output = cJSON_PrintUnformatted(Root);
          cJSON_Delete(Root);
          free(Output);

     return ret;
}

void ClientToServerCmd(unsigned char Cmd)  //发送指令
{
     int ret = 0, val = 0;
     static unsigned int s_Seq = 0;
     char *cJsonBuf;
     char strTime[30];
     cJSON *Root, *cjson_sub;
     unsigned short MsgLen = 0;

     Root = cJSON_CreateObject();
     cjson_sub = cJSON_CreateObject();
     cJSON_AddNumberToObject(Root,"CMD",		 Cmd);
     cJSON_AddNumberToObject(Root,"Dir",		 CMD_MASTER_SEND);
     cJSON_AddNumberToObject(Root,"Seq",		 s_Seq++);
     cJSON_AddItemToObject(Root, "Msg", cjson_sub);

     if(Cmd == CMD_GOTOTAKEAPHOTO || Cmd == CMD_GOTOANGLE) //指定点拍照
     {
          cJSON_AddNumberToObject(cjson_sub, "X", s_Angle_X);

          cJSON_AddNumberToObject(cjson_sub, "Y", s_Angle_Y);

          cJSON_AddNumberToObject(cjson_sub, "Index", s_PhotoIndex);

          cJSON_AddNumberToObject(cjson_sub, "Point", s_PositionPoint);
     }
   
     if(Cmd == CMD_TAKEAPHOTO)
     {
          cJSON_AddNumberToObject(cjson_sub, "Index", s_PhotoIndex);

          cJSON_AddNumberToObject(cjson_sub, "Point", s_PositionPoint);
     }

     cJsonBuf = cJSON_PrintUnformatted(Root);

     MsgLen = strlen(cJsonBuf);

     cJSON_Delete(Root);

     strcat(cJsonBuf, "\r");

     ret = send(clientSocket, cJsonBuf, MsgLen, MSG_NOSIGNAL);

     if(ret < 0)
     {
          printf("Socket 连接已经断开\n");
          shutdown(clientSocket, SHUT_RDWR);
          s_SocketStatus = 0;
     }

     printf("\nClientToServerAlive(%d)(Size:%d):%s\n", ret, MsgLen, cJsonBuf);

     free(cJsonBuf);
}

int GetTCPStatus(void)
{
     struct tcp_info info;
     int len = sizeof(info);

     if(clientSocket>0)
     {
          getsockopt(clientSocket, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
          
          return info.tcpi_state;

     }
}

int getlocalip(char* eth0ip)
{
    int i=0;
    int sockfd;
    struct ifconf ifconf;
    char buf[512];
    struct ifreq *ifreq;
    char stringcmd[100];
    static char s_pppIP[20], s_ConnectFlag = 0;
    // struct ifreq buf[MAXINTERFACES];    
    // struct ifconf ifc;
    char* ip;
    char Count = 0;

    //初始化ifconf
    ifconf.ifc_len = 512;
    ifconf.ifc_buf = buf;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
    {
        return -1;
    }
    ioctl(sockfd, SIOCGIFCONF, &ifconf);    //获取所有接口信息
    close(sockfd);
    //接下来一个一个的获取IP地址
    int if_len = ifconf.ifc_len / sizeof(struct ifreq); 
//     printf("接口数量:%d\n", if_len);
    ifreq = (struct ifreq*)buf;
    
    for(int i=0; i<if_len; i++)
    {
        ip = inet_ntoa(((struct sockaddr_in*)&(ifreq->ifr_addr))->sin_addr);
        // printf("%s:%s\n", ifreq->ifr_name, ip);
        if(memcmp(ifreq->ifr_name, "eth0", 4) == 0)
        {
             strcpy(eth0ip,ip);
             Count++;
        }
        ifreq++;
    }
   
    if(Count >= 1)
    {
        return 0;
    }
    else
    {
        return -1;        
    }

}

int main(int argc, char *argv[])
{
     //客户端只需要一个套接字文件描述符，用于和服务器通信
     int TcpStatus = 0;
     char eth0ip[30] = {0};
     //描述服务器的socket
     struct sockaddr_in serverAddr;
     char sendbuf[MAX_DATA];
     char FileBuf[FILE_MAX];
     int len = 0, Count = 0;
     FILE *fd;

     signal(SIGINT, sig_handler);
     signal(SIGTERM, sig_handler);

     if(argc == 1)	//没有附加参数
	{
		printf(GREEN" 在当前点拍照\n"NONE);
		s_CmdNow = CMD_TAKEAPHOTO;
	}
	else if(argc == 2)	//一个附加参数
	{
		if(strncmp(argv[1], "ret", 3) == 0)	//CMD =6 返回原点指令
		{
			printf(GREEN" 相机返回原点\n"NONE);
			s_CmdNow = CMD_RETURN;
		}
		else if((strncmp(argv[1], "set", 3) == 0) || (strncmp(argv[1], "Set", 3) == 0))	//CMD =7 设置拍照参数
		{
			printf(" 设置拍照参数\n");
			s_CmdNow = CMD_SETPARM;
		}
		else if(strncmp(argv[1], "get", 7) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 获取拍照参数\n");
			s_CmdNow = CMD_GETPARM;
		}
		else if(strncmp(argv[1], "reboot", 6) == 0)	//CMD =7 设置拍照参数
		{
			printf(" 重启设备\n");
			s_CmdNow = CMD_REBOOT;
		}
          else
          {
               sscanf(argv[1],"%d",&s_PhotoIndex);

               if(s_PhotoIndex >= 0 && s_PhotoIndex <= 100000)
               {
                    s_CmdNow = CMD_TAKEAPHOTO;
               }
               else
               {
                    s_CmdNow = 0;
                    printf(" 错误的角度\n");
               }

          }
     	
		// }
	}
	else if(argc == 3)
	{
		sscanf(argv[1],"%d",&s_Angle_X);
		sscanf(argv[2],"%d",&s_Angle_Y);
		printf("Goto X:%d, Y:%d\n", s_Angle_X, s_Angle_Y);

          if(s_Angle_Y >= 0 && s_Angle_Y <= 120 && s_Angle_X >= 0 && s_Angle_X <= 360)
          {
               s_CmdNow = CMD_GOTOTAKEAPHOTO;
          }
          else
          {
               s_CmdNow = 0;
               printf(" 错误的角度\n");
          }
	}
	else if(argc == 4)
	{
		sscanf(argv[1],"%d",&s_Angle_X);
		sscanf(argv[2],"%d",&s_Angle_Y);
          sscanf(argv[3],"%d",&s_PhotoIndex);
          if(strncmp(argv[3], "go", 2) == 0)
          {
               if(s_Angle_Y >= 0 && s_Angle_Y <= 120 && s_Angle_X >= 0 && s_Angle_X <= 360)
               {
                    s_CmdNow = CMD_GOTOANGLE;
               }
               else
               {
                    s_CmdNow = 0;
                    printf(" 错误的角度\n");
               }
          }
          else if(s_PhotoIndex >= 0 && s_PhotoIndex <= 100000)
          {
               printf(GREEN"Goto X:%d, Y:%d (位置点:%d)\n"NONE, s_Angle_X, s_Angle_Y, s_PhotoIndex);

               if(s_Angle_Y >= 0 && s_Angle_Y <= 120 && s_Angle_X >= 0 && s_Angle_X <= 360)
               {
                    s_CmdNow = CMD_GOTOTAKEAPHOTO;
               }
               else
               {
                    s_CmdNow = 0;
                    printf(" 错误的角度\n");
               }
          } 
          else
          {
               printf(" 错误的位置点参数:%s\n", argv[3]);
               s_CmdNow = 0;
          }
	}
     else if(argc == 5)
	{
		sscanf(argv[1],"%d",&s_Angle_X);
		sscanf(argv[2],"%d",&s_Angle_Y);
          sscanf(argv[3],"%d",&s_PhotoIndex);
          sscanf(argv[4],"%d",&s_PositionPoint);
          if(s_PhotoIndex >= 0 && s_PhotoIndex <= 100000 && s_PositionPoint >= 0 && s_PositionPoint <= 10)
          {
               printf(GREEN"Goto X:%d, Y:%d (位置点:%d,角度点:%d)\n"NONE, s_Angle_X, s_Angle_Y, s_PhotoIndex, s_PositionPoint);

               if(s_Angle_Y >= 0 && s_Angle_Y <= 120 && s_Angle_X >= 0 && s_Angle_X <= 360)
               {
                    s_CmdNow = CMD_GOTOTAKEAPHOTO;
               }
               else
               {
                    s_CmdNow = 0;
                    printf(" 错误的角度\n");
               }
          } 
          else
          {
               printf(" 错误的位置点参数:%s\n", argv[3]);
               s_CmdNow = 0;
          }
	}
	else
	{
		printf("app错误的参数\n");
		s_CmdNow = 0;
	}

     if(s_CmdNow == 0)
     {
          return -1;
     }

     getlocalip(eth0ip);

     if((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     {
          perror("socket");
          return -1;
     }
     serverAddr.sin_family = AF_INET;
     serverAddr.sin_port = htons(SERVER_CMD_PORT);
     //指定服务器端的ip，本地测试：127.0.0.1
     //inet_addr()函数，将点分十进制IP转换成网络字节序IP
     
     serverAddr.sin_addr.s_addr = inet_addr(eth0ip);
     printf("服务器地址:%s 端口:%d\n", eth0ip, SERVER_CMD_PORT);
     if(connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
     {
          perror("服务器没有打开");
          return -1;
     }


     s_SocketStatus = 1;
     printf("连接到主机...\n");

     pthread_create(&s_RecvID, NULL, RecvPthread, NULL);
     pthread_detach(s_RecvID);

     ClientToServerCmd(s_CmdNow);

     while(s_AliveAck == 0)
     {
          usleep(1000);
          Count++;
          if(Count >= 1000)
          {
               printf(RED"超时未收到应答\n"NONE);
               break;
          }
     }

     if(s_AliveAck)
     {
          printf(GREEN"收到应答,指令下发成功\n"NONE);
     }
     else
     {
          close(clientSocket);
          return -1;
     }
     
     Count = 0;
     s_AliveAck = 0;
     //转动云台，等待云台执行完成.
     if(s_CmdNow == CMD_GOTOANGLE || s_CmdNow == CMD_GOTOTAKEAPHOTO || s_CmdNow == CMD_TAKEAPHOTO)
     while(s_AliveAck == 0)
     {
          sleep(1);
          Count++;
          if(Count >= 12)
          {
               printf(RED"超时未收到应答,退出\n"NONE);
               break;
          }
     }

     if(s_AliveAck)
     {
          printf(GREEN"执行成功！\n"NONE);
     }

     close(clientSocket);
     return 0;
}
 